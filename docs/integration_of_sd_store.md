[Check previous step](basic_django_project.md)

## Integration of sd_store in a Django project ##

We have already created a Django project and now we can integrate
sd_store on it. In this part of the tutorial we will learn how to
download and configure sd_store in a Django project. When everything is
ready we will run the project in a Django development server.

### Add sd_store as a git submodule ###
To add sd_store to our project first we need to initialize a git repository.
If the project is already a git repository, then skip this step. To make
it a git repository then type the following inside the root folder of the
project (where manage.py is).
```
git init 
```

now we can add sd_store to our project as a submodule at the same level 
as "manage.py" file 
```
git submodule add https://pgallego@bitbucket.org/pgallego/sd_store.git
```



### sd_store libraries installation ###
Now that we have downloaded sd_store let's install the libraries from 
the requirements.txt file. Remember to install them with the virtual 
environment activated. If you do not remember how to do it, 
check the [first part](basic_django_project.md) of the tutorial.
```
pip install -r sd_store/requirements.txt --no-cache-dir
```

The next step is to integrate sd_store in our Django project, 
to do so we will need to modify settings.py and urls.py

###urls.py###
In order to add the urls that sd_store uses to our project we will add
them to the project in its 'urls.py' file. We will set all sd_store urls
to start with '/sdstore/' so that they can be easily identified and also
to avoid any conflict with possible similar urls from our project.

So now open your 'urls.py' file and replace its content with the following:
```
#!python
from django.conf.urls import patterns, include

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
                       (r'^sdstore/', include('sd_store.urls')),
                       (r'^admin/', include(admin.site.urls)),
                       )

```

###settings.py###
In the settings.py file we need to do some modifications in order to
fully integrate sd_store and also to configure our project for a future
deployment. Open the file 'settings.py' file and follow these instructions:

At the top of settings.py file we will define a variable to state 
our projects path:
```
#!python
import os

ROOT_PATH = os.path.dirname(__file__)
```

After that we need to set the database engine to sqlite3 and
define the path where the database will be created. That has to be done
by modifying the existing variable called 'DATABASES', after that it 
should look as it follows:
```
#!python

DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.sqlite3',
            'NAME': os.path.join(ROOT_PATH, '../sqlite.db'),
            'USER': '',
            'PASSWORD': '',
            'HOST': '',
            'PORT': '',
        }
    }
```


In order to define the path where al static files will be collected
search for the variable called 'STATIC_ROOT' and modify its value to
this:
```
#!python
STATIC_ROOT = os.path.join(ROOT_PATH, 'media')
```

The URL in which static files are served is defined by the already 
existing variable 'STATIC_URL'. Search for it and modify it to this:
```
#!python
STATIC_URL = '/media/'
```

Finally add sd_store in the already existing variable 'INSTALLED_APPS': 
(also uncomment 'django.contrib.admin')
```
#!python
INSTALLED_APPS = (
    # other apps
    'sd_store',
    'django.contrib.admin'
)
```

###Database creation###
Now in the command prompt we will create the database by running 
the following command: 
(ensure that you run the command in the location of manage.py file)
```
python manage.py syncdb
```

If you are asked to create a superuser, say "yes" and create it, 
you will need it later in this tutorial.

Now a new file called "sqlite.db" will have appeared in your 
project directory. 

### Run project ###
Everything is ready so that we can run a server with our project.
To do so run the following in a command prompt: 
(ensure that you run the command in the location of manage.py file)
```
python manage.py runserver
```

Open a browser and go to http://127.0.0.1:8000/admin/ address and log in
with the superuser credentials that you defined in the previous step.

Now that our project using sd_store is running we have ended configuring
the server side and we can start acting as a client posting data to that
server that is running sd_store. 
[Continue Tutorial](data_posting_steps.md)
