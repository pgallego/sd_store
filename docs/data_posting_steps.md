[Check previous step](integration_of_sd_store.md)

## Data Posting Steps ##

Now that Django is running a project with sd_store, let's learn
how to interact with a server running sd_store from a client's 
point of view by posting some data.

### Add a user using the admin gui ###
Now that you are in the administration view of Django let's create
a user to start posting data to our sd_store.

In the 'Auth' section add a 'User' called 'tutorial' with a password
'tutorial'.

In the 'sd_store' section add a new 'User profile' select the
'tutorial' user from the dropdown menu and leave everything else blank.

### Authentication ###
From now on we will interact with sd_store using its API.
To do so we will need to create a session first with the user that
we have just created.

But first let's install a couple of libraries in a command prompt. 

From now on we will need to use another command prompt, because the one 
we were using before is running our Django server, and we do not want to 
stop it. 

Remember that the virtual environment needs to be activated when you 
install new libraries.

TIP: To activate virtualenv check the [first part](basic_django_project.md) of the tutorial.

We need to install 'requests' library to call URLs: 
```
pip install requests
```

To interact with the API let's create a python file called 'client.py' 
and save it wherever you want. The following python code has to be added
to 'client.py' in the same order as it is shown here. When the script
'client.py' is completed we will run it. 

Let's start adding code to our client side script, first of all it is
necessary to create a session for the user we previously created.

Add the following to create a session and log in as the user 'tutorial':
```
#!python
import requests
from datetime import datetime, timedelta
import simplejson as json

sd_store_url = 'http://127.0.0.1:8000/'
u_name = 'tutorial'
u_pass = 'tutorial'

# login
login_url = sd_store_url + 'sdstore/login/'

# we need to use the session feature of requests to deal with django login magic
s = requests.Session()

# we need to pass the username and password
login_params = {
    'username': u_name,
    'password': u_pass
}
s.post(login_url, data=login_params)
# we are now logged in
```

That will store the session in the variable called 's', we will use that
session to interact with the API. 

### Sensor and Channel creation ###

In sd_store data readings belong to a sensor, and in that sensor the data
has been read by an specific channel

The sensor has to be created first, add the following to 'client.py':
```
#!python
# create a sensor
# (note that this can also be done from the admin web UI)
sensors_url = sd_store_url + 'sdstore/sensors/'

# "mac" is supposed to be mac address but formatting is not enforced 
# so it can be any text, however needs to be unique
sensor_params = {"mac": '1',
                 "name": '1st sensor',
                 "sensor_type": 'company X sensor'}

r = s.post(sensors_url, data=sensor_params)
#  the response contains the ID of the newly created sensor

sensor_id = int(r.text)
print 'sensor created with id:', sensor_id
# sensor creation done

# get a list of sensors, just to check it worked
r = s.get(sensors_url)
print 'sensor list:', r.text
```

now let's see the code to add a channel to the sensor that we have created,
in this case it will measure temperature. Add the following to 'client.py':
```
#!python
# add a channel, in this example temperature
# (note that this can also be done from the admin web UI)
channel_name = 'temperature'
channel_url = sd_store_url + 'sdstore/sensor/%d/%s/' % (sensor_id, channel_name)

channel_params = {
    "unit": 'degree C',
    "reading_frequency": '120'  # in seconds
}

s.post(channel_url, data=channel_params)
# channel added
```

With a sensor that has a channel created, now we will add the code that
posts data readings to sd_store.

### POST Data ###
The easiest way to post data is to indicate in the URL 
to which channel of which sensor do we want to save it. Then we pass
as a parameter the time and value of the data.

Add the following code to 'client.py':
```
#!python
# post single data point to the newly created channel
data_url = sd_store_url + 'sdstore/sensor/%d/%s/data/' % (sensor_id, channel_name)

# data needs to be expressed as a list of (dictionary) objects 
sensor_data = [{
    'timestamp': datetime.now().strftime('%a %b %d %H:%M:%S %Y'),
    'value': 25.0
}, ]
#  the list needs to be converted to json
dataString = json.dumps(sensor_data)
post_data = {"data": dataString}

r = s.post(data_url, post_data)
# data posted, the result is the number of samples posted
print '1 sample posted, result:', r.text
```

it is also possible to post multiple data points with just one POST 
request. Add the following code to 'client.py':
```
#!python
# post multiple data points to the newly created channel
data_url = sd_store_url + 'sdstore/sensor/%d/%s/data/' % (sensor_id, channel_name)

# data needs to be expressed as a list of (dictionary) objects 
#  NOTE: timestamps need to be distinct (for a given sensor & channel)
sensor_data = [{
    'timestamp': (datetime.now() + timedelta(minutes=1)).strftime('%a %b %d %H:%M:%S %Y'),
    'value': 25.0
}, {
    'timestamp': (datetime.now() + timedelta(minutes=2)).strftime('%a %b %d %H:%M:%S %Y'),
    'value': 25.1
}]
#  the list needs to be converted to json
dataString = json.dumps(sensor_data)
post_data = {"data": dataString}

r = s.post(data_url, post_data)
print '2 samples posted, result:', r.text
# posting multiple data points done
```


### GET Data ###
Getting data from sd_store is intended to offer a visual representation
of the data, and because of that there is a way to do it using 
JavaScript ([Check the documentation](js_data_loader_usage.md)).

However, it is possible to do it through the API doing a GET request.
Add the following code to 'client.py':
```
#!python
# get data back 
data_params = {
    'start': (datetime.now() - timedelta(minutes=60)).strftime('%Y-%m-%d %H:%M:%S'),
    'end': (datetime.now() + timedelta(minutes=60)).strftime('%Y-%m-%d %H:%M:%S'),
    'sampling_interval': '120'
}
r = s.get(data_url, params=data_params)
# should get JSON back
print 'GET data result:', r.text
# done getting data
```

### Run 'client.py' script ###
Now that we have completed the script to POST data to our sd_store 
server we can run it to see the results. Before that, check that your
'client.py' file looks like this:
```
#!python
import requests
from datetime import datetime, timedelta
import simplejson as json

sd_store_url = 'http://127.0.0.1:8000/'
u_name = 'tutorial'
u_pass = 'tutorial'

# login
login_url = sd_store_url + 'sdstore/login/'

# we need to use the session feature of requests to deal with django login magic
s = requests.Session()

# we need to pass the username and password
login_params = {
    'username': u_name,
    'password': u_pass
}
s.post(login_url, data=login_params)
# we are now logged in

# create a sensor
# (note that this can also be done from the admin web UI)
sensors_url = sd_store_url + 'sdstore/sensors/'

# "mac" is supposed to be mac address but formatting is not enforced 
# so it can be any text, however needs to be unique
sensor_params = {"mac": '1',
                 "name": '1st sensor',
                 "sensor_type": 'company X sensor'}

r = s.post(sensors_url, data=sensor_params)
#  the response contains the ID of the newly created sensor

sensor_id = int(r.text)
print 'sensor created with id:', sensor_id
# sensor creation done

# get a list of sensors, just to check it worked
r = s.get(sensors_url)
print 'sensor list:', r.text

# add a channel, in this example temperature
# (note that this can also be done from the admin web UI)
channel_name = 'temperature'
channel_url = sd_store_url + 'sdstore/sensor/%d/%s/' % (sensor_id, channel_name)

channel_params = {
    "unit": 'degree C',
    "reading_frequency": '120'  # in seconds
}

s.post(channel_url, data=channel_params)
# channel added

# post single data point to the newly created channel
data_url = sd_store_url + 'sdstore/sensor/%d/%s/data/' % (sensor_id, channel_name)

# data needs to be expressed as a list of (dictionary) objects 
sensor_data = [{
    'timestamp': datetime.now().strftime('%a %b %d %H:%M:%S %Y'),
    'value': 25.0
}, ]
#  the list needs to be converted to json
dataString = json.dumps(sensor_data)
post_data = {"data": dataString}

r = s.post(data_url, post_data)
# data posted, the result is the number of samples posted
print '1 sample posted, result:', r.text

# post multiple data points to the newly created channel
data_url = sd_store_url + 'sdstore/sensor/%d/%s/data/' % (sensor_id, channel_name)

# data needs to be expressed as a list of (dictionary) objects 
#  NOTE: timestamps need to be distinct (for a given sensor & channel)
sensor_data = [{
    'timestamp': (datetime.now() + timedelta(minutes=1)).strftime('%a %b %d %H:%M:%S %Y'),
    'value': 25.0
}, {
    'timestamp': (datetime.now() + timedelta(minutes=2)).strftime('%a %b %d %H:%M:%S %Y'),
    'value': 25.1
}]
#  the list needs to be converted to json
dataString = json.dumps(sensor_data)
post_data = {"data": dataString}

r = s.post(data_url, post_data)
print '2 samples posted, result:', r.text
# posting multiple data points done

# get data back 
data_params = {
    'start': (datetime.now() - timedelta(minutes=60)).strftime('%Y-%m-%d %H:%M:%S'),
    'end': (datetime.now() + timedelta(minutes=60)).strftime('%Y-%m-%d %H:%M:%S'),
    'sampling_interval': '120'
}
r = s.get(data_url, params=data_params)
# should get JSON back
print 'GET data result:', r.text
# done getting data
```

Ensure that the server is running in another command prompt and that
the virtual environment is activated before writing the following 
command to run the script 'client.py':
```
python client.py
```

Some content will be printed, check the prints in the code of the script
to understand it better.

Using your browser you can check in the Django admin view 
(http://127.0.0.1:8000/admin/sd_store/sensorreading/) that the 
readings have been created successfully.

Congratulations! You have completed the basic tutorial of sd_store.

Now you know the basics to start using sd_store on your own.
For further information about sd_store API check the full documentation:

* [Full sd_store API Documentation](api/api_index.md)

[Back to Index](../README.md)