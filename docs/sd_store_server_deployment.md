# sd_store server deployment in Raspberry pi #

from: http://agiliq.com/blog/2013/08/minimal-nginx-and-gunicorn-configuration-for-djang/

In order to run a sd_store server in a raspberry pi do the following:

1. Install gunicorn: `pip install gunicorn`

2. Install nginx: `sudo apt-get install nginx`

3. Run sd_store in gunicorn from the location of your manage.py file:
```
gunicorn <app_name>.wsgi --bind=127.0.0.1:8001 --daemon
```

4. Set STATIC_ROOT in your django project settings. 
This is where all your static files are going to be collected,
in this case it will be set to: 
```
STATIC_ROOT='/home/me/documents/sdex/sdex/media'
```

5. Collect static files by running: 
```
python manage.py collectstatic
```

6. Set STATIC_URL in your django project settings. 
This is the extension of the url used to request static content,
it will be set to:
```
STATIC_URL='/media/'
```

7. Create a file in ```/etc/nginx/sites-enabled/``` that contains the following:
```
    server{
        listen localhost:8000;
        location / {
            proxy_pass http://127.0.0.1:8001;
        }
        location /media {
            autoindex on;
            alias /home/me/documents/sdex/sdex/media
        }
    }
```

8. Restart nginx by typing the following command: 
```
sudo service nginx restart
```

Now nginx is listening to port 8000, requests go through nginx that acts 
as a reverse proxy server. It filters the requests and if they are
static files then nginx serves them. However, the rest of the requests 
are passed to port 8001 and gunicorn handles them.

If the server needs to be publicly accessible just modify the nginx file 
from step 7 as follows:
```
server{
    listen 80;
    server_name example.com;
    location / {
        proxy_pass http://127.0.0.1:8001;
    }
    location /media {
        autoindex on;
        alias /home/me/documents/sdex/sdex/media
    }
}
```

Now you now how to deploy a production version of a sd_store server in a raspberry pi.

[Back to Index](../README.md)