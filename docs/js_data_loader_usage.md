# JavaScript Data Loader Usage #
sd_store already has a library to load data in JavaScript. That allows
to create a user interface that gets saved data dynamically. If you
have decided to use sd_store to manage your sensor's data you will 
probably be thinking about creating a visualization of that data. Then
you should check how to use this library provided with sd_store.

The location of the library inside sd_store is:
```
sd_store/static/sd_store.dataloader.js
```

## How to use sd_store Data Loader ##
### Session Creation ###
This library can only be used if a session is created in the client that
runs the library. Without authenticating the library will not send the 
necessary cookies to the server. Also note that the session cookies will
not be send if the sd_store server domain is not the same domain of where 
this library is being executed. 

### Parameters ###
In this section we will define  the parameters that the library needs, 
to bring us data back.

#### Compulsory Parameters ####

This parameters have to be provided in every request:

* **url:** the root url that all sd_store urls have in common 
* **start:** the date that defines the beginning of the requested data interval
* **end:** the date that defines the ending of the requested data interval 
* **sampling_interval:** an integer value that represents the sampling interval in seconds

From the next 3 parameters at least and only one has to be included:

* **sensor:** the id of a sensor
* **group:** the id of a sensor group
* **user:** set its value to true 

If sensor is included in the parameters then that will return data
only recorded by that sensor. If group is included in the parameters
the returned data will be from all the sensors of that group. And
finally if user is included in the parameters the data will correspond 
to all the sensors of the current user that has logged in. Remember to 
include only one of these three parameters and at least one of them. 

From the next 3 parameters one has to be included with 'true' as its value:

* **data:** to indicate that the requested data is regular data
* **baseline:** to indicate that the requested data is baseline data
* **integral:** to indicate that the requested data is integral data

#### Optional Parameters ####
The next parameters are optional:

* **channels:** a list of the names of the channels from which the data was recorded
* **annotations:** a boolean value that defines if annotations should be included 
or not in the response data

### Example Code ###

Here you have some examples about the possible usage of the data loader:
```
#!javascript
// Example 1: Data by user  
var parameters = {
    url: 'localhost:8000/sdstore/', // the root from where all sd_store urls have been defined
    start: new Date(2014, 11, 16, 14, 0),
    end: new Date(2014, 12, 2, 14, 0),
    user: true, // this means get the data by user (the user currently logged in)
    data: true, // this means get the readings (rather than the baseline or the integral)
    channels: ['temperature',], // this are the name(s) of the channels (optional)
    sampling_interval: 60*60 // sampling interval in seconds
};

// call the data loader
sd_store.dataloader.load(parameters, function (data) {  
    // TODO: do something with the data
});
```


```
#!javascript
// Example 2: Data by sensor group
var parameters = {
    url: 'localhost:8000/sdstore/', // the root from where all sd_store urls have been defined
    start: new Date(2014, 11, 16, 14, 0),
    end: new Date(2014, 12, 2, 14, 0),
    group: 1, // this means get the data by sensor group
    integral: true, // this means get the readings (rather than the baseline or the data)
    channels: ['temperature',], // this are the name(s) of the channels (optional)
    sampling_interval: 60*60 // sampling interval in seconds
};

// call the data loader
sd_store.dataloader.load(parameters, function (data) {  
    // TODO: do something with the data
});
```

```
#!javascript
// Example 3: Data from only one sensor  
var parameters = {
    url: 'localhost:8000/sdstore/', // the root from where all sd_store urls have been defined
    start: new Date(2014, 11, 16, 14, 0),
    end: new Date(2014, 12, 2, 14, 0),
    sensor: 1, // this means get the data by sensor id
    baseline: true, // this means get the readings (rather than the data or the integral)
    channels: ['temperature',], // this are the name(s) of the channels (optional)
    sampling_interval: 60*60 // sampling interval in seconds
};

// call the data loader
sd_store.dataloader.load(parameters, function (data) {  
    // TODO: do something with the data
});
```

The response of the data loader is a JSON with the following parameters:

* **data:** a list of JSONs representing each of the data readings:
    * **t:** Unix time when the data was recorded (in milliseconds)
    * **value:** the data value recorded 
* **min_datetime:** the date of the first reading in the requested interval
* **max_datetime:** the date of the latest reading in the requested interval

Here you have an example of the structure of the responses:
```
#!javascript
{
    'data': [
                {'t': 1469893351000, 'value': 1}, 
                {'t': 1469893321000, 'value': 0.5}
            ],
    'min_datetime': '2015-03-12 10:57:00',
    'max_datetime': '2016-06-02 14:07:00'
}

```