[Back to API Index](api_index.md)
# Get Channel Last Reading #
Returns the value of the last reading recorded in the requested channel.

##Resource URL##
**/sensor/{sensor_id or mac}/{channel_name}/last-reading/**

###HTTP Verb###
**GET**

## Resource Information ##
* **Requires authentication:** Yes
* **Requires ownership of the resource:** Yes

### Response Structure Example ###
```
#!python
{
    "timestamp": "Sun Aug 07 23:27:51 2016", 
    "value": 12.0
}
```
