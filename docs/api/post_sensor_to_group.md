[Back to API Index](api_index.md)
# Add Sensor to Sensor Group #
Adds a sensor to an existing sensor group.

##Resource URL##
**/sensorGroup/{group_id}/sensors/**

###HTTP Verb###
**POST**

## Resource Information ##
* **Requires authentication:** Yes
* **Requires ownership of the resource:** Yes

## Parameters ##
All parameters are mandatory.

* **sensorID:** the id of the sensor to be added to the group

### Request Parameters Structure Example ###
```
#!python
{
    'sensorID': 1
}
```