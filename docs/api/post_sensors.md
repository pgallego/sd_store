[Back to API Index](api_index.md)
# Create Sensor #
Creates a new sensor and returns the id assigned to it.

##Resource URL##
**/sensors/**

##HTTP Verb##
**POST**

## Resource Information ##
* **Requires authentication:** Yes

## Parameters ##
All parameters are mandatory.

* **mac:** mac address of the sensor
* **name:** name of the sensor
* **sensor_type:** sensor type