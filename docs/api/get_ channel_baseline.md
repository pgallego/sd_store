[Back to API Index](api_index.md)
# Get Channel Baseline #
...

##Resource URL##
**/sensor/{sensor_id}/{channel_name}/baseline/**

###HTTP Verb###
**GET**

## Resource Information ##
* **Requires authentication:** Yes
* **Requires ownership of the resource:** Yes

## Parameters ##
All parameters are mandatory.

* **start:** start date of the period in the the following format 
```'%a %b %d %H:%M:%S %Y'``` (check http://strftime.org/)
* **end:** end date of the period in the the following format 
```'%a %b %d %H:%M:%S %Y'``` (check http://strftime.org/)
* **sampling_interval:** sampling interval in seconds

### Request Parameters Structure Example ###
```
#!python
{
    'start': 'Mon Sep 30 10:00:00 2013',
    'end': 'Mon Sep 30 17:00:00 2013',
    'sampling_interval': 120
}
```