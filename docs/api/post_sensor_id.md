[Back to API Index](api_index.md)
# Update Sensor#
Updates an existing sensor.

##Resource URL##
**/sensor/{sensor_id or mac}**

###HTTP Verb###
**POST**

## Resource Information ##
* **Requires authentication:** Yes
* **Requires ownership of the resource:** Yes

## Parameters ##
All parameters are mandatory.

* **mac:** mac address of the sensor
* **name:** name of the sensor
* **sensor_type:** sensor type