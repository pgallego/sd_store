[Back to API Index](api_index.md)
# Create Channel #
Creates a channel for a sensor. If the channel already exists, then
it is updated.

##Resource URL##
**/sensor/{sensor_id or mac}/{channel_name}/**

##HTTP Verb##
**POST**

## Resource Information ##
* **Requires authentication:** Yes
* **Requires ownership of the resource:** Yes

## Parameters ##
All parameters are mandatory.

* **unit:** unit of measurement
* **reading-frequency:** reading frequency in seconds (integer)