[Back to API Index](api_index.md)
# Delete Channel#
Deletes a channel from a sensor.

##Resource URL##
**/sensor/{sensor_id or mac}/{channel_name}/**

##HTTP Verb##
**DELETE**

## Resource Information ##
* **Requires authentication:** Yes
* **Requires ownership of the resource:** Yes

