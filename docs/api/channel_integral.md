[Back to API Index](api_index.md)
# Get Channel Integral Value #
Returns the integral value (the sum of al values) recorded in the 
requested channel in the indicated period.

##Resource URL##
**/sensor/{sensor_id}/{channel_name}/integral/**

###HTTP Verb###
**GET**

## Resource Information ##
* **Requires authentication:** Yes
* **Requires ownership of the resource:** Yes

## Parameters ##
All parameters are mandatory.

* **start:** start date of the period in the the following format 
```'%a %b %d %H:%M:%S %Y'``` (check http://strftime.org/)
* **end:** end date of the period in the the following format 
```'%a %b %d %H:%M:%S %Y'``` (check http://strftime.org/)

### Request Parameters Structure Example ###
```
#!python
{
    'start': 'Mon Sep 30 10:00:00 2013',
    'end': 'Mon Sep 30 17:00:00 2013'
}
```