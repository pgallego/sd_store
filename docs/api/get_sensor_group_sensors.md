[Back to API Index](api_index.md)
# Get Sensors of SensorGroup #
Returns the information of the sensors that are part of the indicated
sensor group.

##Resource URL##
**sensorGroup/{group_id}/sensors/**

###HTTP Verb###
**GET**

## Resource Information ##
* **Requires authentication:** Yes
* **Requires ownership of the resource:** Yes

### Response Structure Example ###
```
#!python
[
{
    "name": "sensor_1", 
    "mac": "0", 
    "channels": [{"
            reading_frequency": 60, 
            "id": 1, 
            "unit": "degree C", 
            "name": "ch_1"
            }], 
    "sensor_type": "temperature_sensor", 
    "user": "user_1", 
    "id": 1
},
]
```