[Back to API Index](api_index.md)
# Delete Sensor Group #
Deletes a sensor group.

##Resource URL##
**/sensorGroup/{group_id}/**

###HTTP Verb###
**DELETE**

## Resource Information ##
* **Requires authentication:** Yes
* **Requires ownership of the resource:** Yes