[Back to API Index](api_index.md)
# Get SensorGroup #
Returns information about a sensor group.

##Resource URL##
**sensorGroup/{group_id}/**

###HTTP Verb###
**GET**

## Resource Information ##
* **Requires authentication:** Yes
* **Requires ownership of the resource:** Yes

### Response Structure Example ###
```
#!python
{
    "user": "user_1", 
    "sensors":  [
                {
                    "name": "sensor_1", 
                    "mac": "0", 
                    "channels": [{"
                            reading_frequency": 60, 
                            "id": 1, 
                            "unit": "degree C", 
                            "name": "ch_1"
                            }], 
                    "sensor_type": "temperature_sensor", 
                    "user": "user_1", 
                    "id": 1
                },
                ], 
    "description": "The first group", 
    "id": 1, 
    "name": "group_1"
}
```