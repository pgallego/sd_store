[Back to API Index](api_index.md)
# Get Current User's Last Readings #
Returns a list of the last readings of every channel from every sensor
owned by the authenticated user.

##Resource URL##
**/snapshot/**

##HTTP Verb##
**GET**

## Resource Information ##
* **Requires authentication:** Yes