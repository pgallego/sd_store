[Back to API Index](api_index.md)
# Create Sensor Group #
Creates an empty sensor group. The ID of the created group is returned
in the answer to this request. 

##Resource URL##
**/sensorGroups/**

###HTTP Verb###
**POST**

## Resource Information ##
* **Requires authentication:** Yes

## Parameters ##
All parameters are mandatory.

* **name:** the name of the sensor group
* **description:** short description about the sensor group

### Request Parameters Structure Example ###
```
#!python
{
    'name': 'group_1',
    'description': 'This is the first group' 
}
```

### Response Structure Example ###
```
#!python
{
    'id': 1 
}
```