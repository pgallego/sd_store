[Back to API Index](api_index.md)
# Delete Sensor #
Deletes a sensor and also all its readings.

##Resource URL##
**/sensor/{sensor_id or mac}**

###HTTP Verb###
**DELETE**

## Resource Information ##
* **Requires authentication:** Yes
* **Requires ownership of the resource:** Yes