[Back to API Index](api_index.md)
#Upload Data: Option 1#
Upload data recorded in a channel of a sensor. More than one data 
reading point can be uploaded.

##Resource URL##
**/sensor/{sensor_id or mac}/{channel_name}/data/**

###HTTP Verb###
**POST**

## Resource Information ##
* **Requires authentication:** Yes
* **Requires ownership of the resource:** Yes

## Parameters ##
All parameters are mandatory.

* **data:** A list of datums that contain:
    * **timestamp:** time in the the following format 
    ```'%a %b %d %H:%M:%S %Y'``` (check http://strftime.org/)
    * **value:** a float value of the recorded data

### Request Parameters Structure Example ###
```
#!python
{'data':
    [
        {
            'timestamp': 'Mon Sep 30 10:00:00 2013',
            'value': 25.0
        }, 
    ]
}
```

#Upload Data: Option 2#
Upload data recorded in a channel of a sensor. More than one data 
reading point can be uploaded.

##Resource URL##
**/sensor/data/**

###HTTP Verb###
**POST**

## Resource Information ##
* **Requires authentication:** Yes
* **Requires ownership of the resource:** Yes

## Parameters ##
All parameters are mandatory.

* **data:** A list of datums that contain:
    * **timestamp:** time in the the following format 
    ```'%a %b %d %H:%M:%S %Y'``` (check http://strftime.org/)
    * **value:** a float value of the recorded data
    * **sensor:** the mac address of the sensor
    * **channel:** the name of the channel

### Request Parameters Structure Example ###
```
#!python
{'data':
    [
        {
            'timestamp': 'Mon Sep 30 10:00:00 2013',
            'value': 25.0,
            'sensor': '00:0a:95:9d:68:16',
            'channel': 'channel_1' 
        }, 
    ]
}
```

#Upload Data: Option 3#
Upload data recorded in a channel of a sensor. This method requires
that the sensor to which the data is uploaded to be associated with a
Raw Data Key. The key is given as a parameter and due to that no 
authentication is required. 

##Resource URL##
**rawinput/sensor/{sensor_mac}/{channel_name}/data/**

###HTTP Verb###
**POST**

## Resource Information ##
* **Requires authentication:** No
* **Requires ownership of the resource:** No

## Parameters ##
All parameters are mandatory.

* **value:** a float value of the recorded data
* **key:** the key to which the sensor is associated

### Request Parameters Structure Example ###
```
#!python
{
    'value': 25.0,
    'key': 'example_key'
}
```

#Upload Data: Option 4#
Upload data recorded in a channel of a sensor. This method requires
that the sensor to which the data is uploaded to be associated with a
Raw Data Key. The key is given as a parameter and due to that no 
authentication is required. More than one data reading point can be 
uploaded.

##Resource URL##
**rawinput/sensor/data/**

###HTTP Verb###
**POST**

## Resource Information ##
* **Requires authentication:** No
* **Requires ownership of the resource:** No

## Parameters ##
All parameters are mandatory.

* **data:** A list of datums that contain:
    * **value:** a float value of the recorded data
    * **key:** the key to which the sensor is associated
    * **sensor:** the mac address of the sensor
    * **channel:** the name of the channel

### Request Parameters Structure Example ###
```
#!python
{'data':
    [
        {
            'value': 25.0,
            'key': 'example_key',
            'sensor': '00:0a:95:9d:68:16',
            'channel': 'channel_1' 
        }, 
    ]
}
```