[Back to API Index](api_index.md)
# Get Sensor Group Data #
Returns the data recorded by every channel in every sensor of the 
group in the specified period.

##Resource URL##
**/sensorGroup/{group_id}/data**

###HTTP Verb###
**GET**

## Resource Information ##
* **Requires authentication:** Yes
* **Requires ownership of the resource:** Yes

## Parameters ##
All parameters are mandatory.

* **start:** start date of the period in the the following format 
```'%a %b %d %H:%M:%S %Y'``` (check http://strftime.org/)
* **end:** end date of the period in the the following format 
```'%a %b %d %H:%M:%S %Y'``` (check http://strftime.org/)
* **sampling_interval:** sampling interval in seconds

### Request Parameters Structure Example ###
```
#!python
{
    'start': 'Mon Sep 30 10:00:00 2013',
    'end': 'Mon Sep 30 17:00:00 2013',
    'sampling_interval': 120
}
```

### Response Structure Example ###
```
#!python
[{
    'sensor': '00:0a:95:9d:68:16',
    'channels_data':
    {
    'data':
        [{
            't': 1469893351,
            'value': 25.0
        }, ],
    'min_datetime': '2013-09-30 11:00:00',
    'max_datetime': '2013-09-30 16:00:00',
    'channel': 'channel_1'
    }
},]
```