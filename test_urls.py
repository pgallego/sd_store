# -*- coding: UTF-8 -*-

# This file is part of sd_store
# 
# sd_store is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# sd_store is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with sd_store.  If not, see <http://www.gnu.org/licenses/>.

from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns('',
                       url(r'^accounts/login/$', 'login_view', name='auth_login'),
                       url(r'^accounts/logout/$', 'logout_view', name='auth_logout'),

                       (r'^admin/', include(admin.site.urls)),
                       (r'^', include('sd_store.urls')),
                       # the following two are dummy urls / views just for testing
                       # TODO: check why it is needed
                       url(r'^$', 'sensors_view', name='home_view'),
                       url(r'^profile/$', 'sensors_view', name='profile_view'),
                       )
