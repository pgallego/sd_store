# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Channel'
        db.create_table('sd_store_channel', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=32)),
            ('unit', self.gf('django.db.models.fields.CharField')(max_length=10)),
            ('reading_frequency', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('sd_store', ['Channel'])

        # Adding model 'Sensor'
        db.create_table('sd_store_sensor', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('mac', self.gf('django.db.models.fields.CharField')(unique=True, max_length=30, db_index=True)),
            ('sensor_type', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
        ))
        db.send_create_signal('sd_store', ['Sensor'])

        # Adding M2M table for field channels on 'Sensor'
        m2m_table_name = db.shorten_name('sd_store_sensor_channels')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('sensor', models.ForeignKey(orm['sd_store.sensor'], null=False)),
            ('channel', models.ForeignKey(orm['sd_store.channel'], null=False))
        ))
        db.create_unique(m2m_table_name, ['sensor_id', 'channel_id'])

        # Adding model 'SensorGroup'
        db.create_table('sd_store_sensorgroup', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=1024, blank=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
        ))
        db.send_create_signal('sd_store', ['SensorGroup'])

        # Adding M2M table for field sensors on 'SensorGroup'
        m2m_table_name = db.shorten_name('sd_store_sensorgroup_sensors')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('sensorgroup', models.ForeignKey(orm['sd_store.sensorgroup'], null=False)),
            ('sensor', models.ForeignKey(orm['sd_store.sensor'], null=False))
        ))
        db.create_unique(m2m_table_name, ['sensorgroup_id', 'sensor_id'])

        # Adding model 'EepromSensorReading'
        db.create_table('sd_store_eepromsensorreading', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('timestamp', self.gf('django.db.models.fields.DateTimeField')(db_index=True)),
            ('sensor', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['sd_store.Sensor'])),
            ('channel', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['sd_store.Channel'])),
            ('value', self.gf('django.db.models.fields.FloatField')(default=0)),
            ('index', self.gf('django.db.models.fields.IntegerField')(default=0)),
        ))
        db.send_create_signal('sd_store', ['EepromSensorReading'])

        # Adding unique constraint on 'EepromSensorReading', fields ['timestamp', 'sensor', 'channel']
        db.create_unique('sd_store_eepromsensorreading', ['timestamp', 'sensor_id', 'channel_id'])

        # Adding model 'SensorReading'
        db.create_table('sd_store_sensorreading', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('timestamp', self.gf('django.db.models.fields.DateTimeField')(db_index=True)),
            ('sensor', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['sd_store.Sensor'])),
            ('channel', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['sd_store.Channel'])),
            ('value', self.gf('django.db.models.fields.FloatField')(default=0)),
        ))
        db.send_create_signal('sd_store', ['SensorReading'])

        # Adding unique constraint on 'SensorReading', fields ['timestamp', 'sensor', 'channel']
        db.create_unique('sd_store_sensorreading', ['timestamp', 'sensor_id', 'channel_id'])

        # Adding model 'RawDataKey'
        db.create_table('sd_store_rawdatakey', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('value', self.gf('django.db.models.fields.CharField')(max_length=32)),
        ))
        db.send_create_signal('sd_store', ['RawDataKey'])

        # Adding M2M table for field sensors on 'RawDataKey'
        m2m_table_name = db.shorten_name('sd_store_rawdatakey_sensors')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('rawdatakey', models.ForeignKey(orm['sd_store.rawdatakey'], null=False)),
            ('sensor', models.ForeignKey(orm['sd_store.sensor'], null=False))
        ))
        db.create_unique(m2m_table_name, ['rawdatakey_id', 'sensor_id'])

        # Adding model 'Baseline'
        db.create_table('sd_store_baseline', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('date', self.gf('django.db.models.fields.DateField')(db_index=True)),
            ('sensor', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['sd_store.Sensor'])),
            ('channel', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['sd_store.Channel'])),
            ('value', self.gf('django.db.models.fields.FloatField')()),
            ('last_modified', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
        ))
        db.send_create_signal('sd_store', ['Baseline'])

        # Adding unique constraint on 'Baseline', fields ['date', 'sensor', 'channel']
        db.create_unique('sd_store_baseline', ['date', 'sensor_id', 'channel_id'])

        # Adding model 'EventType'
        db.create_table('sd_store_eventtype', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=30)),
            ('icon', self.gf('django.db.models.fields.URLField')(max_length=200)),
            ('alt_icon', self.gf('django.db.models.fields.URLField')(max_length=200)),
        ))
        db.send_create_signal('sd_store', ['EventType'])

        # Adding model 'EventTypePrediction'
        db.create_table('sd_store_eventtypeprediction', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('event_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['sd_store.EventType'])),
            ('certainty', self.gf('django.db.models.fields.FloatField')()),
            ('user_accepted', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('sd_store', ['EventTypePrediction'])

        # Adding model 'Event'
        db.create_table('sd_store_event', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('event_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['sd_store.EventType'], null=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=1024)),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=1024, blank=True)),
            ('start', self.gf('django.db.models.fields.DateTimeField')()),
            ('end', self.gf('django.db.models.fields.DateTimeField')()),
            ('sensor', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['sd_store.Sensor'])),
            ('channel', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['sd_store.Channel'])),
            ('auto_detected', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('_maximum_power', self.gf('django.db.models.fields.FloatField')(null=True, blank=True)),
            ('_minimum_power', self.gf('django.db.models.fields.FloatField')(null=True, blank=True)),
            ('_mean_power', self.gf('django.db.models.fields.FloatField')(null=True, blank=True)),
            ('_standard_deviation', self.gf('django.db.models.fields.FloatField')(null=True, blank=True)),
        ))
        db.send_create_signal('sd_store', ['Event'])

        # Adding M2M table for field predictions on 'Event'
        m2m_table_name = db.shorten_name('sd_store_event_predictions')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('event', models.ForeignKey(orm['sd_store.event'], null=False)),
            ('eventtypeprediction', models.ForeignKey(orm['sd_store.eventtypeprediction'], null=False))
        ))
        db.create_unique(m2m_table_name, ['event_id', 'eventtypeprediction_id'])

        # Adding model 'Goal'
        db.create_table('sd_store_goal', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=30, blank=True)),
            ('start', self.gf('django.db.models.fields.DateTimeField')()),
            ('end', self.gf('django.db.models.fields.DateTimeField')()),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('consumption', self.gf('django.db.models.fields.FloatField')()),
        ))
        db.send_create_signal('sd_store', ['Goal'])

        # Adding model 'UserProfile'
        db.create_table('sd_store_userprofile', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'], unique=True)),
            ('primary_sensor', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['sd_store.Sensor'], null=True, blank=True)),
            ('phone_number', self.gf('django.db.models.fields.CharField')(max_length=32, null=True, blank=True)),
        ))
        db.send_create_signal('sd_store', ['UserProfile'])

        # Adding model 'StudyInfo'
        db.create_table('sd_store_studyinfo', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'], unique=True)),
            ('baseline_consumption', self.gf('django.db.models.fields.FloatField')()),
            ('start_date', self.gf('django.db.models.fields.DateTimeField')()),
            ('last_modified', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('initial_credit', self.gf('django.db.models.fields.FloatField')()),
        ))
        db.send_create_signal('sd_store', ['StudyInfo'])

        # Adding model 'DetectionLog'
        db.create_table('sd_store_detectionlog', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('sensor', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['sd_store.Sensor'])),
            ('timestamp', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('comment', self.gf('django.db.models.fields.CharField')(max_length=256)),
        ))
        db.send_create_signal('sd_store', ['DetectionLog'])


    def backwards(self, orm):
        # Removing unique constraint on 'Baseline', fields ['date', 'sensor', 'channel']
        db.delete_unique('sd_store_baseline', ['date', 'sensor_id', 'channel_id'])

        # Removing unique constraint on 'SensorReading', fields ['timestamp', 'sensor', 'channel']
        db.delete_unique('sd_store_sensorreading', ['timestamp', 'sensor_id', 'channel_id'])

        # Removing unique constraint on 'EepromSensorReading', fields ['timestamp', 'sensor', 'channel']
        db.delete_unique('sd_store_eepromsensorreading', ['timestamp', 'sensor_id', 'channel_id'])

        # Deleting model 'Channel'
        db.delete_table('sd_store_channel')

        # Deleting model 'Sensor'
        db.delete_table('sd_store_sensor')

        # Removing M2M table for field channels on 'Sensor'
        db.delete_table(db.shorten_name('sd_store_sensor_channels'))

        # Deleting model 'SensorGroup'
        db.delete_table('sd_store_sensorgroup')

        # Removing M2M table for field sensors on 'SensorGroup'
        db.delete_table(db.shorten_name('sd_store_sensorgroup_sensors'))

        # Deleting model 'EepromSensorReading'
        db.delete_table('sd_store_eepromsensorreading')

        # Deleting model 'SensorReading'
        db.delete_table('sd_store_sensorreading')

        # Deleting model 'RawDataKey'
        db.delete_table('sd_store_rawdatakey')

        # Removing M2M table for field sensors on 'RawDataKey'
        db.delete_table(db.shorten_name('sd_store_rawdatakey_sensors'))

        # Deleting model 'Baseline'
        db.delete_table('sd_store_baseline')

        # Deleting model 'EventType'
        db.delete_table('sd_store_eventtype')

        # Deleting model 'EventTypePrediction'
        db.delete_table('sd_store_eventtypeprediction')

        # Deleting model 'Event'
        db.delete_table('sd_store_event')

        # Removing M2M table for field predictions on 'Event'
        db.delete_table(db.shorten_name('sd_store_event_predictions'))

        # Deleting model 'Goal'
        db.delete_table('sd_store_goal')

        # Deleting model 'UserProfile'
        db.delete_table('sd_store_userprofile')

        # Deleting model 'StudyInfo'
        db.delete_table('sd_store_studyinfo')

        # Deleting model 'DetectionLog'
        db.delete_table('sd_store_detectionlog')


    models = {
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'sd_store.baseline': {
            'Meta': {'unique_together': "(('date', 'sensor', 'channel'),)", 'object_name': 'Baseline'},
            'channel': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['sd_store.Channel']"}),
            'date': ('django.db.models.fields.DateField', [], {'db_index': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_modified': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'sensor': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['sd_store.Sensor']"}),
            'value': ('django.db.models.fields.FloatField', [], {})
        },
        'sd_store.channel': {
            'Meta': {'object_name': 'Channel'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '32'}),
            'reading_frequency': ('django.db.models.fields.IntegerField', [], {}),
            'unit': ('django.db.models.fields.CharField', [], {'max_length': '10'})
        },
        'sd_store.detectionlog': {
            'Meta': {'object_name': 'DetectionLog'},
            'comment': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'sensor': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['sd_store.Sensor']"}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        'sd_store.eepromsensorreading': {
            'Meta': {'ordering': "['timestamp']", 'unique_together': "(('timestamp', 'sensor', 'channel'),)", 'object_name': 'EepromSensorReading'},
            'channel': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['sd_store.Channel']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'index': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'sensor': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['sd_store.Sensor']"}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {'db_index': 'True'}),
            'value': ('django.db.models.fields.FloatField', [], {'default': '0'})
        },
        'sd_store.event': {
            'Meta': {'object_name': 'Event'},
            '_maximum_power': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            '_mean_power': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            '_minimum_power': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            '_standard_deviation': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'auto_detected': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'channel': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['sd_store.Channel']"}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '1024', 'blank': 'True'}),
            'end': ('django.db.models.fields.DateTimeField', [], {}),
            'event_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['sd_store.EventType']", 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '1024'}),
            'predictions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['sd_store.EventTypePrediction']", 'null': 'True', 'blank': 'True'}),
            'sensor': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['sd_store.Sensor']"}),
            'start': ('django.db.models.fields.DateTimeField', [], {})
        },
        'sd_store.eventtype': {
            'Meta': {'object_name': 'EventType'},
            'alt_icon': ('django.db.models.fields.URLField', [], {'max_length': '200'}),
            'icon': ('django.db.models.fields.URLField', [], {'max_length': '200'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'sd_store.eventtypeprediction': {
            'Meta': {'object_name': 'EventTypePrediction'},
            'certainty': ('django.db.models.fields.FloatField', [], {}),
            'event_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['sd_store.EventType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'user_accepted': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        'sd_store.goal': {
            'Meta': {'object_name': 'Goal'},
            'consumption': ('django.db.models.fields.FloatField', [], {}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'end': ('django.db.models.fields.DateTimeField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'start': ('django.db.models.fields.DateTimeField', [], {}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']"})
        },
        'sd_store.rawdatakey': {
            'Meta': {'object_name': 'RawDataKey'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'sensors': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['sd_store.Sensor']", 'symmetrical': 'False'}),
            'value': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        },
        'sd_store.sensor': {
            'Meta': {'object_name': 'Sensor'},
            'channels': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['sd_store.Channel']", 'symmetrical': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mac': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30', 'db_index': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'sensor_type': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']"})
        },
        'sd_store.sensorgroup': {
            'Meta': {'object_name': 'SensorGroup'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '1024', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'sensors': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['sd_store.Sensor']", 'symmetrical': 'False'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']"})
        },
        'sd_store.sensorreading': {
            'Meta': {'ordering': "['timestamp']", 'unique_together': "(('timestamp', 'sensor', 'channel'),)", 'object_name': 'SensorReading'},
            'channel': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['sd_store.Channel']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'sensor': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['sd_store.Sensor']"}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {'db_index': 'True'}),
            'value': ('django.db.models.fields.FloatField', [], {'default': '0'})
        },
        'sd_store.studyinfo': {
            'Meta': {'object_name': 'StudyInfo'},
            'baseline_consumption': ('django.db.models.fields.FloatField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'initial_credit': ('django.db.models.fields.FloatField', [], {}),
            'last_modified': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'start_date': ('django.db.models.fields.DateTimeField', [], {}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']", 'unique': 'True'})
        },
        'sd_store.userprofile': {
            'Meta': {'object_name': 'UserProfile'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'phone_number': ('django.db.models.fields.CharField', [], {'max_length': '32', 'null': 'True', 'blank': 'True'}),
            'primary_sensor': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['sd_store.Sensor']", 'null': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']", 'unique': 'True'})
        }
    }

    complete_apps = ['sd_store']