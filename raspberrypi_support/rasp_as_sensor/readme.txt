In order to use a raspberry pi as a sensor:
    - Install the libraries defined in rasp_sensor_requirements.txt
    - (Optional) Follow the example shown in api_population_demo.py to populate the database
    - Save cron_script.py in your raspberry pi
    - Run cron_jobs.py once to create a cron job that calls cron_script.py periodically