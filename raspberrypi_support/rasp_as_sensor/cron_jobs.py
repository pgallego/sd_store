# encoding: utf-8

# This file is part of sd_store
#
# sd_store is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# sd_store is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with sd_store.  If not, see <http://www.gnu.org/licenses/>.

"""
This file contains the configuration of the cron file to generate periodic actions.
The only variables that need to be modified here are:
    - The 'env' is the python executable that is going to be used to run the script
    - The 'script' is the file that is going to be executed periodically

For more information about the usage of python-crontab visit:
    https://pypi.python.org/pypi/python-crontab/
"""
from crontab import CronTab
tab = CronTab(user=True)
env = '.../pve/rasp_sensor/local/bin/python2.7'  # Set the path to your environment
script = '.../cron_script.py'  # Set the path to the script
cmd = env + ' ' + script
interval = 2

job = tab.new(cmd, comment='job_1')
job.minute.every(interval)  # Check the documentation to learn how to use different intervals
#  job.enable(False)  # uncomment to stop the job
tab.write()
