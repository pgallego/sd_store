# encoding: utf-8

# This file is part of sd_store
#
# sd_store is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# sd_store is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with sd_store.  If not, see <http://www.gnu.org/licenses/>.

"""
This script contains example code to POST random data to sd_store server.
In combination with 'cron_jobs.py' these 2 files are sufficient to use a raspberry pi
device as a sensor that periodically sends data to a server that runs sd_store.

The only variables that need to be modified here are:
    - sd_store_url: the root of the url where sd_store is running
    - user: the user that owns the sensor
    - password: the password of that user
    - sensor_id: the id of the sensor to which the data belongs to
    - channel_name: the name of the channel from which the data has been recorded
"""
import requests
from datetime import datetime
import simplejson as json
import random

sd_store_url = 'http://127.0.0.1:8000/'
user = 'demo'
password = '123'
sensor_id = 1
channel_name = 'temperature'


# This function returns a session for the indicated user
def login(u_name, u_pass):
    login_url = sd_store_url + 'sdstore/login/'
    # we need to use the session feature of requests to deal with django login magic
    s = requests.Session()

    # we need to pass the username and password
    login_params = {
        'username': u_name,
        'password': u_pass
    }
    s.post(login_url, data=login_params)
    return s


# This function posts the value to the channel of a sensor with that moments time
def post_data(s, value, s_id, ch_name):
    # post single data point to the newly created channel
    data_url = sd_store_url + 'sdstore/sensor/%d/%s/data/' % (s_id, ch_name)

    # data needs to be expressed as a list of (dictionary) objects
    sensor_data = [{
        'timestamp': datetime.now().strftime('%a %b %d %H:%M:%S %Y'),
        'value': value
    }, ]

    #  the list needs to be converted to json
    data_string = json.dumps(sensor_data)
    data = {"data": data_string}
    s.post(data_url, data)


# This function generates a random value, ONLY FOR TESTING PURPOSES
def random_value():
    return random.randrange(-20, 50)

# POST called
post_data(login(user, password), random_value(), sensor_id, channel_name)
