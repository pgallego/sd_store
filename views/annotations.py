# -*- coding: UTF-8 -*-

# This file is part of sd_store
# 
# sd_store is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# sd_store is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with sd_store.  If not, see <http://www.gnu.org/licenses/>.

from django.http import HttpResponse, HttpResponseBadRequest, HttpResponseForbidden, \
    HttpResponseNotFound
from django.shortcuts import get_object_or_404
from django.views.decorators.csrf import csrf_exempt

from sd_store.sdutils import djutils
from sd_store.sdutils.decorators import access_required
from ..models import Annotation, SensorChannelPair
from sd_store.sdutils.djutils import get_json_error, get_json_success
from ..forms import IntervalForm, AnnotationForm
from django.utils import simplejson as json
from sd_store.sdutils.djutils import to_dict


@csrf_exempt
@access_required
def annotation_view(request, annotation_id=None):
    owner = djutils.get_requested_user(request)
    if request.method == "GET":
        if annotation_id is not None:
            annotation = get_object_or_404(Annotation, id=annotation_id)
            if annotation.user != djutils.get_requested_user(request):
                return HttpResponseForbidden(
                    "Tried to access an annotation "
                    "from a different user than the authenticated user.")
            return HttpResponse(json.dumps(to_dict(annotation)))
        else:
            # annotation_id not specified
            # get all annotations within interval
            form = IntervalForm(request.GET)
            if not form.is_valid():
                return HttpResponseBadRequest(get_json_error(dict(form.errors)))

            start = form.cleaned_data['start']
            end = form.cleaned_data['end']

            if start >= end:
                return HttpResponseBadRequest(get_json_error('invalid interval requested'))

            # filter by user
            annotations = Annotation.objects.filter(user=owner)
            annotations = annotations.filter(end__gte=start)
            annotations = annotations.filter(start__lte=end)
            result = [to_dict(annotation) for annotation in annotations]
            return HttpResponse(json.dumps(result))

    elif request.method == "POST":
        annotation_owner = djutils.get_requested_user(request)

        annotation = None
        if annotation_id is not None:
            try:
                annotation = Annotation.objects.get(id=annotation_id)
            except Annotation.DoesNotExist:  # If it doesn't exist, then throw 404
                return HttpResponseNotFound(
                    "The specified sensor does not exist: " + repr(annotation_id))
            if annotation.user != annotation_owner:
                return HttpResponseForbidden(
                    "Tried to modify an annotation that doesn't belong the authenticated user.")
        try:
            in_pairs = json.loads(request.POST.get('pairs'))
        except KeyError:
            return HttpResponseBadRequest("Pairs are missing.")
        except TypeError:
            return HttpResponseBadRequest("The data is not a well formed JSON string.")

        pairs = []
        for in_pair in in_pairs:
            try:
                pair = SensorChannelPair.objects.get(sensor=in_pair['sensor']['id'],
                                                     channel=in_pair['channel']['id'])
            except SensorChannelPair.DoesNotExist:
                return HttpResponseNotFound(get_json_error("invalid pair"))
            pairs.append(pair)

        if len(pairs) == 0:
            return HttpResponseBadRequest(get_json_error("no pairs for annotation"))

        form = AnnotationForm(request.POST, instance=annotation)
        if not form.is_valid():
            return HttpResponseBadRequest(get_json_error(dict(form.errors)))

        annotation = form.save(commit=False)
        annotation.user = owner
        annotation.save()
        annotation.pairs = pairs
        annotation.save()
        if annotation_id is not None:
            return HttpResponse(get_json_success(annotation.id), status=200)
        else:
            return HttpResponse(get_json_success(annotation.id), status=201)
    elif request.method == "DELETE":
        if annotation_id is None:
            return HttpResponse("Not allowed to delete all sensors with a single call", status=403)
        user = djutils.get_requested_user(request)
        try:
            annotation = Annotation.objects.get(pk=annotation_id)
            if user != annotation.user:
                return HttpResponseForbidden("Cannot delete other users' annotation!")
            else:
                annotation.delete()
                return HttpResponse("Annotation %s deleted." % (annotation_id,), status=200)
        except Annotation.DoesNotExist:
            return HttpResponseNotFound("Annotation with id %s not found." % (annotation_id,))
    else:
        return HttpResponseBadRequest(get_json_error("NOT_GET_POST_OR_DELETE_REQUEST"))
