# sd_store Documentation Index #

Welcome to sd_store, the sensor data library for Django.

## Basic Tutorial ##
In this tutorial you will learn how to integrate sd_store sensor data
library in a Django project and how to interact with its API. 

The basic tutorial will approximately take you about 30 minutes.

Follow the next three steps in order to complete the tutorial:

1. [Basic Django project to run sd_store](docs/basic_django_project.md)
2. [Integration of sd_store in a Django project](docs/integration_of_sd_store.md)
3. [Data posting steps](docs/data_posting_steps.md)

## sd_store Documentation ##

### Full sd_store API Documentation ###

* [Create Sensor](docs/api/post_sensors.md)
* [Update Sensor](docs/api/post_sensor_id.md)
* [Delete Sensor](docs/api/delete_sensor.md)
* [Get Sensors](docs/api/get_sensors.md)
* [Create Channel](docs/api/post_sensor_id_channel.md)
* [Delete Channel](docs/api/delete_sensor_id_channel.md)
* [Get Channel Integral Value](docs/api/channel_integral.md)
* [Get Channel Baseline](docs/api/get_channel_baseline.md)
* [Get Channel Last Reading](docs/api/get_channel_last_reading.md)
* [Upload Data Methods](docs/api/upload_data.md)
* [Get Current User's Last Readings](docs/api/user_all_last_readings.md)
* [Create Sensor Group](docs/api/post_sensor_group.md)
* [Get SensorGroup](docs/api/get_sensor_group.md)
* [Get SensorGroup Sensors](docs/api/get_sensor_group_sensors.md)
* [Delete Sensor Group](docs/api/delete_sensor_group.md)
* [Add Sensor to Sensor Group](docs/api/post_sensor_to_group.md)
* [Delete Sensor from Sensor Group](docs/api/delete_sensor_from_group.md)
* [Get Sensor Group Data](docs/api/get_sensor_group_data.md)

### JavaScript sd_store Data Loader Library ###
* [sd_store data loading with JavaScript](docs/js_data_loader_usage.md)

### Raspberry Pi Support ###
* [Deployment of sd_store server in Raspberry pi](docs/sd_store_server_deployment.md)
* [Raspberry pi used as a sensor](docs/rasp_as_sensor.md)