# -*- coding: UTF-8 -*-

# This file is part of sd_store
# 
# sd_store is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# sd_store is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with sd_store.  If not, see <http://www.gnu.org/licenses/>.

from django.conf.urls import patterns  # , url

# general.py view urls
urlpatterns = patterns('sd_store.views.general',
                       (r'^login/$', 'login_view'),
                       (r'^logout/$', 'logout_view'),
                       (r'^users/$', 'user_view'),
                       # (r'^user/(?P<user_id>[-\w]+)/$','user_view'),

                       (r'^sensors/$', 'sensor_view'),
                       (r'^sensor/(?P<sensor_id>[-\d]+)/$', 'sensor_view'),
                       (r'^sensor/(?P<sensor_id>[-\d]+)/(?P<channel_name>[-\w]+)/$',
                        'channel_view'),
                       (r'^sensor/(?P<sensor_id>[-\d]+)/(?P<channel_name>[-\w]+)/data/$',
                        'data_view'),
                       (r'^sensor/data/$', 'batch_data_view'),
                       (r'^sensor/(?P<sensor_id>[-\d]+)/(?P<channel_name>[-\w]+)/last-reading/$',
                        'last_reading_view'),
                       (r'^sensor/(?P<sensor_id>[-\d]+)/(?P<channel_name>[-\w]+)/integral/$',
                        'integral_view'),
                       (r'^sensor/(?P<sensor_id>[-\d]+)/(?P<channel_name>[-\w]+)/baseline/$',
                        'baseline_view'),

                       (r'^snapshot/$', 'snapshot_view'),

                       (r'^sensor/(?P<sensor_mac>[-\w:]+)/$', 'sensor_view_mac'),
                       (r'^sensor/(?P<sensor_mac>[-\w:]+)/(?P<channel_name>[-\w]+)/$',
                        'channel_view_mac'),
                       (r'^sensor/(?P<sensor_mac>[-\w:]+)/(?P<channel_name>[-\w]+)/data/$',
                        'data_view_mac'),
                       (r'^sensor/(?P<sensor_mac>[-\w:]+)/(?P<channel_name>[-\w]+)/last-reading/$',
                        'last_reading_view_mac'),

                       (r'^rawinput/sensor/(?P<sensor_mac>[-\w:]+)/(?P<channel_name>[-\w]+)/data/',
                        'raw_data_view'),
                       (r'^rawinput/sensor/(?P<sensor_mac>[-\w:]+)/data/', 'raw_data_packet_view'),
                       (r'^rawinput/sensor/(?P<sensor_id>[\d]+)/register/',
                        'raw_data_register_view'),
                       (r'^rawinput/sensor/(?P<sensor_mac>[-\w:]+)/signal/',
                        'raw_data_signal_view'),
                       (r'^rawinput/sensor/data/$', 'raw_batch_data_view'),

                       (r'^sensorGroups/$', 'sensor_group_list_view'),
                       (r'^sensorGroup(s?)/(?P<sensor_group_id>[\d]+)/$',
                        'sensor_group_detail_view'),
                       (r'^sensorGroup(s?)/(?P<sensor_group_id>[\d]+)/sensors/$',
                        'sensor_group_sensors_list_view'),
                       (r'^sensorGroup(s?)/(?P<sensor_group_id>[\d]+)/sensor/'
                        r'(?P<sensor_id>[-\d]+)/$', 'sensor_group_sensor_detail_view'),
                       (r'^sensorGroup(s?)/(?P<sensor_group_id>[\d]+)/data/$', 'group_data_view'),
                       # (r'^sensorGroup(s?)/(?P<sensor_group_id>[\d]+)/(?P<channel_name>[\w]+)
                       # /data/$','group_data_view'),

                       (r'^eventTypes/$', 'event_type_view'),
                       (r'^eventType/(?P<event_type_id>[\d]+)/$', 'event_type_view'),

                       (r'^referenceConsumption/$', 'reference_consumption_view'),
                       # (r'^goals','goal_view'),
                       # (r'^goal/(?P<goal_id>[-\w]+)/$','goal_view'),
                       )

# energy.py view urls
urlpatterns += patterns('sd_store.views.energy',
                        # energy version
                        (r'^energy/data/$', 'sensor_reading_view', {'data_type': 'energy'}),

                        (r'^energy/alwaysOn/', 'always_on_view', {'data_type': 'energy'}),
                        (r'^energy/total/', 'total_energy_view'),

                        (r'^energy/totalCost/', 'total_energy_cost_view'),

                        # power version
                        (r'^power/data/$', 'sensor_reading_view', {'data_type': 'power'}),

                        (r'^power/alwaysOn/$', 'always_on_view', {'data_type': 'power'}),

                        # general
                        (r'^eventNames/$', 'event_names_view'),
                        (r'^events/$', 'event_view'),
                        (r'^event/(?P<event_id>[\d]+)/$', 'event_view'),
                        (r'^event/$', 'event_view'),

                        (r'^liveStats/$', 'live_stats_view'),
                        (r'^savings/$', 'savings_view'),
                        )

# external.py view urls
urlpatterns += patterns('sd_store.views.external',
                        (r'^powerNow/$', 'power_now_view'),
                        (r'^update/$', 'update_view'),
                        (r'^checkAlertMeLogin/$', 'check_alertme_login_view'),
                        )

# annotations.py view urls
urlpatterns += patterns('sd_store.views.annotations',
                        (r'^annotations/$', 'annotation_view'),
                        (r'^annotation/(?P<annotation_id>[\d]+)/$', 'annotation_view'),
                        (r'^annotation/$', 'annotation_view'),
                        )
