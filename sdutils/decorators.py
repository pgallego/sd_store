# -*- coding: UTF-8 -*-

# This file is part of sd_store
#
# sd_store is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# sd_store is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with sd_store.  If not, see <http://www.gnu.org/licenses/>.

"""
Created on 22 Dec 2011

@author: enrico
"""
import urlparse
from django.conf import settings
from django.http import HttpResponseForbidden
from django.utils.decorators import available_attrs
from django.contrib.auth.decorators import REDIRECT_FIELD_NAME
from django.contrib.auth.models import User
from djutils import get_json_error, can_access_user_data
from logging import getLogger

logger = getLogger('custom')

try:
    from functools import wraps
except ImportError:
    from django.utils.functional import wraps  # Python 2.4 fallback.


def access_required(view_func):
    login_url = None
    redirect_field_name = REDIRECT_FIELD_NAME

    @wraps(view_func, assigned=available_attrs(view_func))
    def _wrapped_view(request, *args, **kwargs):
        if request.user.is_authenticated():
            # does the request contain a user?
            owner = request.REQUEST.get('user')
            # can the user access the data?
            if can_access_user_data(owner, request.user):
                return view_func(request, *args, **kwargs)
            else:
                return HttpResponseForbidden(get_json_error('ACCESS_DENIED'))
        path = request.build_absolute_uri()
        # If the login url is the same scheme and net location then just
        # use the path as the "next" url.
        login_scheme, login_netloc = urlparse.urlparse(login_url or
                                                       settings.LOGIN_URL)[:2]
        current_scheme, current_netloc = urlparse.urlparse(path)[:2]
        if ((not login_scheme or login_scheme == current_scheme) and
                (not login_netloc or login_netloc == current_netloc)):
            path = request.get_full_path()
        from django.contrib.auth.views import redirect_to_login
        return redirect_to_login(path, login_url, redirect_field_name)

    return _wrapped_view


def log_request(view_func):
    @wraps(view_func, assigned=available_attrs(view_func))
    def _wrapped_view(request, *args, **kwargs):
        user_id = request.user.id
        try:
            user = User.objects.get(id=user_id)
        except Exception, e:
            logger.debug('Exception: ' + str(e))
            user = str(user_id)
        # user = 'undef'

        params_dict = {}
        if request.method == "POST":
            params_dict = request.POST
        elif request.method == "GET":
            params_dict = request.GET
        else:
            pass

        params = ', '.join(['"%s": "%s"' % (k, v) for k, v in params_dict.items()])

        msg = '"user": "%s", "view": "%s", "%s": {%s}' % (
            user, view_func.__name__, request.method, params)
        logger.info(msg)

        return view_func(request, *args, **kwargs)

    return _wrapped_view
