# -*- coding: UTF-8 -*-

# This file is part of sd_store
#
# sd_store is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# sd_store is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with sd_store.  If not, see <http://www.gnu.org/licenses/>.
from django.test import TestCase
from django.test.client import Client

from datetime import datetime, timedelta
from django.contrib.auth.models import User
from django.utils import simplejson
from django.db.models import Min

from sd_store.models import SensorReading, Event, EventType, UserProfile, Channel, Sensor, \
    SensorChannelPair

from math import sin, pi
from numpy import std

from sd_store.sdutils.general import total_seconds, moving_average
from sd_store.sdutils.djutils import DATE_FMTS

from sd_store.sdutils.store_utils import NoPrimaryMeterException

# JavaScript date format
# JS_FMT = '%Y-%m-%dT%H:%M:%S.%f'
# JS_FMT = '%a %b %d %H:%M:%S %Y'
JS_FMT = '%Y-%m-%d %H:%M:%S'


class LocalTestCase(TestCase):
    urls = 'sd_store.test_urls'


def energy_f(x):
    return 0.2 + 1.0 + sin(2 * pi * x / 60.0)


def generate_energy_data(user, current_dt):
    profile = UserProfile.objects.get(user=user)
    meter_one = profile.primary_sensor
    channel = filter(lambda x: x.name == 'energy', meter_one.channels.all())[0]

    data_days = 10
    s = current_dt - timedelta(days=data_days)

    from itertools import cycle
    # create sinusoidal data
    hour_data = [energy_f(x) for x in range(0, 60, 2)]
    data_source = cycle(hour_data)
    all_values = []
    for t in (s + timedelta(minutes=m) for m in range(0, 60 * 24 * data_days, 2)):
        v = data_source.next()
        data_point = SensorReading(timestamp=t, sensor=meter_one, channel=channel, value=v)
        data_point.save()
        all_values.append(v)

    from sd_store.sdutils.store_utils import ALWAYS_ON_WINDOW_SIZE
    smoothed = moving_average(all_values, ALWAYS_ON_WINDOW_SIZE)

    always_on = min(smoothed)
    return always_on


class EnergyTest(LocalTestCase):
    # fixtures = ['sd_store_data', 'sd_store_users', 'sd_store_energy']
    fixtures = ['sd_store_data', 'auth_users', 'sd_store_users']

    def setUp(self):
        TestCase.setUp(self)

        self.username = 'user3@example.com'
        self.password = 'test'

        # print User.objects.all()
        self.user = User.objects.get(username=self.username)

        self.now = datetime(2013, 4, 4, 18)
        self.always_on = generate_energy_data(self.user, self.now)

        user_sensors = Sensor.objects.filter(user=self.user)
        self.sensor = filter(lambda x: x.sensor_type == 'MeterReader', user_sensors)[0]
        channel = Channel.objects.get(name='energy')

        e_type = EventType.objects.get(name='generic')
        start_dt = SensorReading.objects.aggregate(Min('timestamp'))['timestamp__min']
        start_dt = datetime(start_dt.year, start_dt.month, start_dt.day, start_dt.hour, 2)
        start = start_dt + timedelta(hours=3)
        end = start + timedelta(minutes=18)
        self.pair = SensorChannelPair(sensor=self.sensor, channel=channel)
        self.pair.save()
        event_one = Event(name='event_one', event_type=e_type, start=start, end=end)
        event_one.save()
        event_one.pairs.add(self.pair)

        start = start + timedelta(hours=2, minutes=10)
        end = start + timedelta(minutes=30)
        event_two = Event(name='event two', event_type=e_type, start=start, end=end)
        event_two.save()
        event_two.pairs.add(self.pair)

        logged_in = self.client.login(username=self.username, password=self.password)

        # This is for the always_on test, but can be used elsewhere.
        self.opClient = Client()
        print self.opClient
        print "EC:", User.objects.filter(username="user3@example.com").count()
        print "OP:", User.objects.filter(username="user4@example.com").count()
        for user in User.objects.all():
            print repr(user)
        op_logged_in = self.opClient.login(username="user4@example.com", password="test")
        print op_logged_in

        print 'logged_in:', logged_in, "OP Logged in:", op_logged_in

        self.longMessage = True

    def test_event_names(self):
        response = self.client.get('/eventNames/')

        self.assertEqual(response.status_code, 200)
        parsed = simplejson.loads(str(response.content))
        self.assertEqual(len(parsed), 2)

    def test_energy_data(self):
        # with no arguments, it should fail
        response = self.client.get('/energy/data/')

        self.assertEqual(response.status_code, 400)
        parsed = simplejson.loads(str(response.content))
        self.assertEqual(len(parsed['error']['reason'].keys()), 3)

        # pass bad arguments
        start = 'str((self.now - timedelta(days=7)))'
        end = 'str(self.now)'
        sampling_interval = 120
        response = self.client.get('/energy/data/',
                                   {'start': start, 'end': end,
                                    'sampling_interval': sampling_interval})
        self.assertEqual(response.status_code, 400)
        parsed = simplejson.loads(str(response.content))
        self.assertEqual(len(parsed['error']['reason'].keys()), 2)

        # pass correct arguments
        now = self.now - timedelta(hours=1)
        start = now - timedelta(days=7)
        end = now
        # use JavaScript date format
        start = start.strftime(JS_FMT)
        end = end.strftime(JS_FMT)
        sampling_interval = 120

        # wrong parameters
        response = self.client.get('/energy/data/',
                                   {'start': end,
                                    'end': start,
                                    'sampling_interval': sampling_interval})
        self.assertEqual(response.status_code, 400)

        # sensor with no readings
        no_data_sensor = Sensor.objects.filter(user=self.user).exclude(id=self.sensor.id).all()[0]
        response = self.client.get('/energy/data/',
                                   {'start': start,
                                    'end': end,
                                    'sampling_interval': sampling_interval,
                                    'm_ID': no_data_sensor.id})

        self.assertEqual(response.status_code, 200)
        parsed = simplejson.loads(str(response.content))
        self.assertEqual(parsed['min_datetime'], 0)
        self.assertEqual(parsed['max_datetime'], 0)

        # correct params
        response = self.client.get('/energy/data/',
                                   {'start': start,
                                    'end': end,
                                    'sampling_interval': sampling_interval})

        self.assertEqual(response.status_code, 200)
        parsed = simplejson.loads(str(response.content))
        data = parsed['data']
        self.assertEqual(len(data), 7 * 24 * 30)
        min_value = min([x['value'] for x in data])

        # print [x['value'] for x in data]
        #        from matplotlib import pyplot as plt
        #        plt.figure()
        #        plt.plot([x['value'] for x in data])
        #        plt.show()
        self.assertGreaterEqual(min_value, 0.0)
        for i, m in enumerate(range(0, 60, 2)):
            self.assertEqual(data[i]['value'], energy_f(m), 'm: %d, i: %d' % (m, i))

        # test re-sampling
        start = self.now - timedelta(hours=8)
        end = self.now - timedelta(hours=2)
        # use JavaScript date format
        sampling_interval = 60 * 60
        response = self.client.get('/energy/data/',
                                   {'start': start.strftime(JS_FMT),
                                    'end': end.strftime(JS_FMT),
                                    'sampling_interval': sampling_interval})

        self.assertEqual(response.status_code, 200)
        parsed = simplejson.loads(str(response.content))
        data = parsed['data']

        min_value = min([x['value'] for x in data])
        self.assertGreaterEqual(min_value, 0.0)

        self.assertEqual(len(data), 6)
        self.assertEqual(len(data), total_seconds(end - start) / sampling_interval)
        for i in range(1, len(data)):
            self.assertEqual(data[i - 1]['value'], data[i]['value'])
        self.assertAlmostEqual(data[0]['value'], sum([energy_f(t) for t in range(0, 60, 2)]))

    def test_power_data(self):
        # pass correct arguments
        start = self.now - timedelta(days=7)
        end = self.now
        # use JavaScript date format
        start = start.strftime(JS_FMT)
        end = end.strftime(JS_FMT)
        sampling_interval = 120
        response = self.client.get('/power/data/', {'start': start, 'end': end,
                                                    'sampling_interval': sampling_interval})

        self.assertEqual(response.status_code, 200)
        parsed = simplejson.loads(str(response.content))
        data = parsed['data']
        self.assertEqual(len(data), 7 * 24 * 30)
        power_factor = 60 * 60 / sampling_interval
        for i, m in enumerate(range(0, 60, 2)):
            self.assertEqual(data[i]['value'], energy_f(m) * power_factor)

        # test re-sampling
        start = self.now - timedelta(hours=8)
        end = self.now - timedelta(hours=2)
        # use JavaScript date format
        sampling_interval = 60 * 60
        response = self.client.get('/power/data/',
                                   {'start': start.strftime(JS_FMT),
                                    'end': end.strftime(JS_FMT),
                                    'sampling_interval': sampling_interval})

        self.assertEqual(response.status_code, 200)
        parsed = simplejson.loads(str(response.content))
        data = parsed['data']

        self.assertEqual(len(data), 6)
        self.assertEqual(len(data), total_seconds(end - start) / sampling_interval)
        for i in range(1, len(data)):
            self.assertEqual(data[i - 1]['value'], data[i]['value'])

        power_factor = 60 * 60.0 / 120
        avg_power = power_factor * sum([energy_f(t) for t in range(0, 60, 2)]) / 30.0
        self.assertAlmostEqual(data[0]['value'], avg_power)

        # print ["%s -> %.2f"%(datetime.fromtimestamp(t), v) for t, v in data]

    def test_always_on(self):

        # without parameters it should fail
        response = self.client.get('/energy/alwaysOn/')
        self.assertEqual(response.status_code, 400)
        parsed = simplejson.loads(str(response.content))
        self.assertEqual(len(parsed['error']['reason'].keys()), 3)

        # with incorrect parameters it should fail
        response = self.client.get('/energy/alwaysOn/',
                                   {'start': str(datetime.now()),
                                    'end': 'end',
                                    'sampling_interval': 120})
        self.assertEqual(response.status_code, 400)
        parsed = simplejson.loads(str(response.content))
        self.assertEqual(len(parsed['error']['reason'].keys()), 2)

        # pass correct parameters (energy)
        start = self.now - timedelta(days=2)
        end = self.now

        # sensor with no readings
        no_data_sensor = Sensor.objects.filter(user=self.user).exclude(id=self.sensor.id).all()[0]
        response = self.client.get('/energy/alwaysOn/',
                                   {'start': start,
                                    'end': end,
                                    'sampling_interval': 120,
                                    'm_ID': no_data_sensor.id})
        self.assertEqual(response.status_code, 200)

        # use JavaScript date format
        start = start.strftime(JS_FMT)
        end = end.strftime(JS_FMT)

        # user with no primary meter:
        self.assertRaises(NoPrimaryMeterException, lambda: self.opClient.get('/energy/alwaysOn/',
                                                                             {'start': start,
                                                                              'end': end,
                                                                              'sampling_interval':
                                                                                  120})
                          )

        # user with primary meter:
        response = self.client.get('/energy/alwaysOn/',
                                   {'start': start,
                                    'end': end,
                                    'sampling_interval': 120})
        self.assertEqual(response.status_code, 200)

        parsed = simplejson.loads(str(response.content))

        self.assertEqual(len(parsed), 2 * 24 * 30)
        values = [p['value'] for p in parsed]
        for v in values:
            self.assertAlmostEqual(v, parsed[0]['value'], 5)

        self.assertAlmostEqual(parsed[0]['value'], self.always_on)

        # get energy at half resolution
        response = self.client.get('/energy/alwaysOn/',
                                   {'start': start, 'end': end, 'sampling_interval': 240})
        self.assertEqual(response.status_code, 200)

        parsed = simplejson.loads(str(response.content))
        # print 'parsed:', parsed
        self.assertEqual(len(parsed), 2 * 24 * 15)
        self.assertAlmostEqual(parsed[0]['value'], 2 * self.always_on)

        # get power
        response = self.client.get('/power/alwaysOn/',
                                   {'start': start, 'end': end, 'sampling_interval': 120})
        self.assertEqual(response.status_code, 200)

        parsed = simplejson.loads(str(response.content))
        # print 'parsed:', parsed
        self.assertEqual(len(parsed), 2 * 24 * 30)
        self.assertAlmostEqual(parsed[0]['value'], self.always_on * 30.0)

        # get power at half resolution
        response = self.client.get('/power/alwaysOn/',
                                   {'start': start, 'end': end, 'sampling_interval': 240})
        self.assertEqual(response.status_code, 200)

        parsed = simplejson.loads(str(response.content))
        # print 'parsed:', parsed
        self.assertEqual(len(parsed), 2 * 24 * 15)
        self.assertAlmostEqual(parsed[0]['value'], self.always_on * 30.0)

    def test_total(self):
        print 'test_total'

        start = self.now - timedelta(days=3)
        end = self.now - timedelta(days=1)
        # use JavaScript date format
        start = start.strftime(JS_FMT)
        end = end.strftime(JS_FMT)
        response = self.client.get('/energy/total/', {'start': start, 'end': end})
        self.assertEqual(response.status_code, 200)

        parsed = simplejson.loads(str(response.content))
        # print 'parsed:', parsed
        self.assertAlmostEqual(parsed, 2 * 24 * sum([energy_f(t) for t in range(0, 60, 2)]))

    def test_total_cost(self):
        print 'test_total_cost'

        start = self.now - timedelta(days=3)
        end = self.now - timedelta(days=1)
        # use JavaScript date format
        start = start.strftime(JS_FMT)
        end = end.strftime(JS_FMT)
        # Wrong interval
        response = self.client.get('/energy/totalCost/', {'start': end, 'end': start})
        self.assertEqual(response.status_code, 400)

        # Right interval
        response = self.client.get('/energy/totalCost/', {'start': start, 'end': end})
        self.assertEqual(response.status_code, 200)

        parsed = simplejson.loads(str(response.content))
        print 'parsed:', parsed
        # self.assertAlmostEqual(parsed['total'], 2*24*36.0)
        total = parsed['total_cost']
        always_on = parsed['always_on_cost']
        variable = parsed['variable_load_cost']
        self.assertGreaterEqual(total, 0.0)
        self.assertGreaterEqual(always_on, 0.0)
        self.assertGreaterEqual(variable, 0.0)
        self.assertEqual(variable, total - always_on)

    def test_events(self):
        # without parameters it should fail
        response = self.client.get('/events/')
        self.assertEqual(response.status_code, 400)
        parsed = simplejson.loads(str(response.content))
        self.assertEqual(len(parsed['error']['reason'].keys()), 2)

        # with incorrect parameters it should fail
        response = self.client.get('/events/', {'start': str(33), 'end': 'end'})
        self.assertEqual(response.status_code, 400)
        parsed = simplejson.loads(str(response.content))
        self.assertEqual(len(parsed['error']['reason'].keys()), 2)

        # pass correct parameters
        start = self.now - timedelta(days=11)
        end = self.now
        # use JavaScript date format
        start = start.strftime(JS_FMT)
        end = end.strftime(JS_FMT)
        all_items_response = self.client.get('/events/', {'start': start, 'end': end})
        self.assertEqual(all_items_response.status_code, 200)

        parsed = simplejson.loads(str(all_items_response.content))
        self.assertEqual(len(parsed), 2)

        for event in parsed:
            start = event['start']
            end = event['end']
            start = datetime.strptime(start, DATE_FMTS[0])
            end = datetime.strptime(end, DATE_FMTS[0])
            duration = end - start
            duration = total_seconds(duration) / 60
            self.assertGreater(duration, 0)

            ev_range = range(int(start.minute), int(start.minute + duration), 2)
            consumption = sum([energy_f(x) for x in ev_range])
            always_on = self.always_on * (duration / 2)
            print 'consumption:', consumption
            print 'always_on:', always_on, event['total_always_on']

            import logging
            logger = logging.getLogger()
            logger.info('event:' + str(event))

            # self.assertAlmostEqual(event['net_consumption'], consumption - always_on)
            self.assertAlmostEqual(event['net_consumption'],
                                   consumption - event['total_always_on'])

            # self.assertGreaterEqual(event['cost'], 0)

        meter_response = self.client.get('/event/%d/' % (parsed[0]['id']))
        self.assertEqual(meter_response.status_code, 200)

        # test that the response can be deserialized
        simplejson.loads(str(meter_response.content))

        # try to create a new event

        # without arguments it should fail
        response = self.client.post('/event/')
        self.assertEqual(response.status_code, 400)
        parsed = simplejson.loads(str(response.content))
        self.assertEqual(len(parsed['error']['reason'].keys()), 5)

        ev_data = {'start': 'now', 'end': 'end', 'name': 'test1', 'event_type_id': '3'}

        # with bad arguments it should fail
        response = self.client.post('/event/', ev_data)
        self.assertEqual(response.status_code, 400)
        parsed = simplejson.loads(str(response.content))
        self.assertEqual(len(parsed['error']['reason'].keys()), 3)

        # with good arguments it should work
        start = SensorReading.objects.aggregate(Min('timestamp'))['timestamp__min'] + timedelta(
            hours=48)
        end = start + timedelta(minutes=45)
        # use JavaScript date format
        start = start.strftime(JS_FMT)
        end = end.strftime(JS_FMT)
        ev_data = {'start': start,
                   'end': end,
                   'name': 'test1',
                   'event_type_id': '3',
                   'pairs': self.pair.id
                   }
        response = self.client.post('/event/', ev_data)
        self.assertEqual(response.status_code, 200)
        parsed = simplejson.loads(str(response.content))

        ev_id = parsed['id']

        # duplication should fail
        start = SensorReading.objects.aggregate(Min('timestamp'))['timestamp__min'] + timedelta(
            hours=3)
        end = start + timedelta(minutes=18)
        # use JavaScript date format
        start = start.strftime(JS_FMT)
        end = end.strftime(JS_FMT)
        ev_data = {'start': start, 'end': end, 'name': 'test1', 'event_type_id': '3'}

        response = self.client.post('/event/', ev_data)
        self.assertEqual(response.status_code, 400)

        # check that there is one more event
        start = self.now - timedelta(days=11)
        end = self.now

        # use JavaScript date format
        start = start.strftime(JS_FMT)
        end = end.strftime(JS_FMT)
        all_items_response = self.client.get('/events/', {'start': start, 'end': end})
        self.assertEqual(all_items_response.status_code, 200)

        parsed = simplejson.loads(str(all_items_response.content))
        self.assertEqual(len(parsed), 3)

        # remove the event
        response = self.client.delete('/event/%d/' % ev_id)
        self.assertEqual(response.status_code, 200)
        simplejson.loads(str(response.content))

        # check that there is one less event
        all_items_response = self.client.get('/events/', {'start': start, 'end': end})
        self.assertEqual(all_items_response.status_code, 200)

        parsed = simplejson.loads(str(all_items_response.content))
        self.assertEqual(len(parsed), 2)

    def test_live_stats(self):
        response = self.client.get('/liveStats/')

        self.assertEqual(response.status_code, 200)
        parsed = simplejson.loads(str(response.content))
        self.assertEqual(len(parsed), 6)

        # generate data for today
        generate_energy_data(self.user, datetime.now())

        response = self.client.get('/liveStats/')

        self.assertEqual(response.status_code, 200)
        parsed = simplejson.loads(str(response.content))
        self.assertEqual(len(parsed), 6)

    def test_backwards_events(self):
        # Should fail if an event with a negative duration passed without producing a 400 error.

        # Set main bounds for the test
        main_start_datetime = self.now - timedelta(days=11)
        main_end_datetime = self.now
        # use JavaScript date format
        main_start = main_start_datetime.strftime(JS_FMT)
        main_end = main_end_datetime.strftime(JS_FMT)

        # Find how many events are in the DB to begin with
        all_items_response = self.client.get('/events/', {'start': main_start, 'end': main_end})
        self.assertEqual(all_items_response.status_code, 200)
        parsed = simplejson.loads(str(all_items_response.content))
        initial_count = len(parsed)

        # get the sensor and channel
        all_sensors_response = self.client.get('/sensors/')
        self.assertEqual(all_sensors_response.status_code, 200)

        all_sensors = simplejson.loads(str(all_sensors_response.content))
        sensor = filter(lambda x: x['sensor_type'] == 'MeterReader', all_sensors)[0]
        channel = filter(lambda x: x['name'] == 'energy', sensor['channels'])[0]

        start = main_start_datetime - timedelta(hours=48)
        end = start - timedelta(minutes=45)  # End is before start
        # use JavaScript date format
        start = start.strftime(JS_FMT)
        end = end.strftime(JS_FMT)
        ev_data = {'start': start,
                   'end': end,
                   'name': 'test1',
                   'event_type_id': '3',
                   'sensor': sensor['id'],
                   'channel': channel['id']}

        response = self.client.post('/event/', ev_data)
        parsed = simplejson.loads(str(response.content))
        print parsed['error']['reason']
        self.assertEqual(response.status_code, 400)

        # Find how many events are in the DB at the end
        all_items_response = self.client.get('/events/', {'start': main_start, 'end': main_end})
        self.assertEqual(all_items_response.status_code, 200)
        parsed = simplejson.loads(str(all_items_response.content))
        final_count = len(parsed)

        # Check there are as many events at beginning as end
        print "Initial count:", initial_count, "Final count:", final_count
        self.assertEqual(initial_count, final_count)

    def test_stdev(self):
        e_type = EventType.objects.get(name='generic')
        start_dt = SensorReading.objects.aggregate(Min('timestamp'))['timestamp__min']
        # start_dt = datetime(start_dt.year, start_dt.month, start_dt.day, start_dt.hour, 2)
        start = start_dt + timedelta(hours=3)
        end = start + timedelta(minutes=60)
        event = Event(name='event_one', event_type=e_type, start=start, end=end)
        event.save()
        event.pairs.add(self.pair)

        expected = std([energy_f(m) for m in range(0, 60, 2)])

        self.assertEqual(event.standard_deviation, expected)
