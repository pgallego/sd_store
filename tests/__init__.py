# -*- coding: UTF-8 -*-

# This file is part of sd_store
#
# sd_store is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# sd_store is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with sd_store.  If not, see <http://www.gnu.org/licenses/>.

# until django 1.6: http://stackoverflow.com/questions/6248510/how-to-spread-django-unit-tests-over-multiple-files
# from django 1.6: http://stackoverflow.com/questions/5160688/organizing-django-unit-tests/20932450#20932450
import unittest


def suite():
    return unittest.TestLoader().discover("sd_store.tests", pattern="*.py")

# run tests with:
# python manage.py test sd_store/tests/test_annotations_view.py:Class.function
