# -*- coding: UTF-8 -*-

# This file is part of sd_store
#
# sd_store is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# sd_store is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with sd_store.  If not, see <http://www.gnu.org/licenses/>.
import json

from django.test import TestCase

from datetime import datetime, timedelta
from django.contrib.auth.models import User
from django.utils import simplejson

from sd_store.models import SensorReading, UserProfile, Channel, Sensor, RawDataKey

from math import sin, pi

from sd_store.sdutils.general import total_seconds, moving_average
from nose.tools import nottest

# JavaScript date format
# JS_FMT = '%Y-%m-%dT%H:%M:%S.%f'
# JS_FMT = '%a %b %d %H:%M:%S %Y'
JS_FMT = '%Y-%m-%d %H:%M:%S'


class LocalTestCase(TestCase):
    urls = 'sd_store.test_urls'


class PushTest(LocalTestCase):
    fixtures = ['auth_users']

    def setUp(self):
        TestCase.setUp(self)

        self.username = 'user3@example.com'
        self.password = 'test'
        self.user = User.objects.get(username=self.username)
        self.username_2 = 'user2@example.com'
        self.user_2 = User.objects.get(username=self.username_2)
        self.mac = "04:0c:ce:d0:a3:aa"
        self.wrong_mac = "01:01:01:01:01:01"
        self.ch = Channel(
            name='test_ch',
            unit='dec C',
            reading_frequency=60)
        self.ch.save()
        self.s = Sensor(
            mac=self.mac,
            sensor_type="test",
            name="temperature",
            user=self.user
        )
        self.s.save()
        self.s.channels.add(self.ch)
        self.s.save()
        self.ch2 = Channel(
            name='test_ch_2',
            unit='dec C',
            reading_frequency=60)
        self.ch2.save()
        self.s2 = Sensor(
            mac="00:00:00:00:00:00",
            sensor_type="test2",
            name="temperature",
            user=self.user
        )
        self.s2.save()
        self.s2.channels.add(self.ch2)
        self.s2.save()
        self.ch3 = Channel(
            name='test_ch_3',
            unit='dec C',
            reading_frequency=60)
        self.ch3.save()
        self.s3 = Sensor(
            mac="00:00:00:00:00:01",
            sensor_type="test3",
            name="temperature",
            user=self.user_2
        )
        self.s3.save()
        self.s3.channels.add(self.ch3)
        self.s3.save()

        self.ch4 = Channel(
            name='rssi',
            unit='dBm',
            reading_frequency=60)
        self.ch4.save()
        self.s4 = Sensor(
            mac="00:00:00:00:00:02",
            sensor_type="test4",
            name="test",
            user=self.user
        )
        self.s4.save()
        self.s4.channels.add(self.ch4)
        self.s4.save()

        self.k = RawDataKey(value=self.s.mac)
        self.k.save()
        self.k.sensors.add(self.s)
        self.k.sensors.add(self.s3)

    def test_raw_batch_data(self):
        self.client.login(username=self.username, password=self.password)

        url = '/rawinput/sensor/data/'
        test_list = [{
            'timestamp': 'Mon Aug 1 10:00:00 2016',
            'value': 0.1,
            'channel': self.ch.name,
            'sensor': self.mac,
            'key': self.k.value
        }]
        test_list_2 = [{
            'timestamp': 'Mon Aug 1 10:00:00 2016',
            'value': 0.1,
            'channel': self.ch2.name,
            'sensor': self.s2.mac,
            'key': self.k.value
        }]
        test_list_3 = [{
            'timestamp': 'Mon Aug 1 10:00:00 2016',
            'value': 0.1,
            'channel': self.ch3.name,
            'sensor': self.s3.mac,
            'key': self.k.value
        }]
        wrong_channel_list = [{
            'timestamp': 'Mon Aug 1 10:00:00 2016',
            'value': 0.1,
            'channel': self.ch2.name,
            'sensor': self.mac,
            'key': self.k.value
        }]
        wrong_mac_list = [{
            'timestamp': 'Mon Aug 1 10:00:00 2016',
            'value': 0.1,
            'channel': self.ch.name,
            'sensor': '0',
            'key': self.k.value
        }]
        wrong_key_list = [{
            'timestamp': 'Mon Aug 1 10:00:00 2016',
            'value': 0.1,
            'channel': self.ch.name,
            'sensor': self.mac,
            'key': '0'
        }]
        key_error_list = [{'error': ''}]
        value_error_list = {
            'timestamp': '10:00:00 2010/01/01',
            'value': 0.1,
            'channel': self.ch.id,
            'sensor': self.s.id,
            'key': self.k.value
        }
        test_data = {'data': simplejson.dumps(test_list)}
        test_data_2 = {'data': simplejson.dumps(test_list_2)}
        test_data_3 = {'data': simplejson.dumps(test_list_3)}
        wrong_channel_data = {'data': simplejson.dumps(wrong_channel_list)}
        key_error_data = {'data': simplejson.dumps(key_error_list)}
        value_error_data = {'data': simplejson.dumps([value_error_list])}
        list_error_data = {'data': simplejson.dumps(value_error_list)}
        mac_error_data = {'data': simplejson.dumps(wrong_mac_list)}
        raw_key_error_data = {'data': simplejson.dumps(wrong_key_list)}

        # post with no data: 400
        response = self.client.post(url)
        self.assertEqual(response.status_code, 400)

        # ToDo: post with bad formed json: 400
        # bad_formed_data = {}
        # response = self.client.post(url, bad_formed_data)
        # self.assertEqual(response.status_code, 400)

        # key error in data: 400
        response = self.client.post(url, key_error_data)
        self.assertEqual(response.status_code, 400)

        # value error in data: 400
        response = self.client.post(url, value_error_data)
        self.assertEqual(response.status_code, 400)

        # data not a list: 400
        response = self.client.post(url, list_error_data)
        self.assertEqual(response.status_code, 400)

        # wrong raw key: 401
        response = self.client.post(url, raw_key_error_data)
        self.assertEqual(response.status_code, 401)

        # correct raw key but with unrelated sensor: 401
        response = self.client.post(url, test_data_2)
        self.assertEqual(response.status_code, 401)

        # correct raw key but with other user's sensor: 403
        response = self.client.post(url, test_data_3)
        self.assertEqual(response.status_code, 403)

        # non existent sensor: 404
        response = self.client.post(url, mac_error_data)
        self.assertEqual(response.status_code, 404)

        # non existent channel for sensor: 404
        response = self.client.post(url, wrong_channel_data)
        self.assertEqual(response.status_code, 404)

        # invalid request: 405
        response = self.client.get(url)
        self.assertEqual(response.status_code, 405)

        # correct data: 200
        response = self.client.post(url, test_data)
        self.assertEqual(response.status_code, 200)

        # post correct data again to check update: 200
        response = self.client.post(url, test_data)
        self.assertEqual(response.status_code, 200)

    def test_raw_data(self):
        url = '/rawinput/sensor/%s/%s/data/' % (self.s.mac, self.ch.name)
        url_2 = '/rawinput/sensor/%s/%s/data/' % (self.s.mac, 'wrong_channel')

        # post raw data with the right key: 200
        data = {"value": 0.1, "key": self.k.value}
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 200)

        # post wrong raw data with the right key: 400
        data = {"value": 'a', "key": self.k.value}
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 400)

        # post raw data with the right key again: 200
        data = {"value": 0.1, "key": self.k.value}
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 200)

        # post data to non existent channel: 404
        data = {"value": 0.1, "key": self.k.value}
        response = self.client.post(url_2, data)
        self.assertEqual(response.status_code, 404)

        # post raw data with the wrong key: 401
        data = {"value": 0.1, "key": '1'}
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 401)

    def test_raw_data_register(self):
        url = '/rawinput/sensor/%d/register/' % self.s.id
        url2 = '/rawinput/sensor/%d/register/' % 0

        # no mac provided: 400
        data = {}
        response = self.client.get(url, data)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.content, 'No MAC address provided')

        # wrong sensor id: 404
        data = {}
        response = self.client.get(url2, data)
        self.assertEqual(response.status_code, 404)

        # get right mac and sensor id
        data = {'mac': self.mac}
        response = self.client.get(url, data)
        self.assertEqual(response.status_code, 200)

        # get wrong mac and right sensor id
        data = {'mac': self.wrong_mac}
        response = self.client.get(url, data)
        self.assertEqual(response.status_code, 404)

    def test_raw_data_signal(self):
        url = '/rawinput/sensor/%s/signal/' % self.s4.mac
        url_2 = '/rawinput/sensor/%s/signal/' % self.s.mac
        url_3 = '/rawinput/sensor/%s/signal/' % '0'

        # wrong mac provided: 404
        data = {}
        response = self.client.get(url_3, data)
        self.assertEqual(response.status_code, 404)

        # no rssi provided: 400
        data = {'a': ''}
        response = self.client.get(url, data)
        self.assertEqual(response.status_code, 400)

        # no rssi channel: 404
        data = {'rssi': 1}
        response = self.client.get(url_2, data)
        self.assertEqual(response.status_code, 404)

        # correct data: 200
        data = {'rssi': 1}
        response = self.client.get(url, data)
        self.assertEqual(response.status_code, 200)

    # ToDo: Finish coverage for raw_data_packet_view
    def test_raw_data_packet(self):
        url = '/rawinput/sensor/%s/data/' % self.s.mac
        url_2 = '/rawinput/sensor/%s/data/' % '0'

        # wrong mac: 404
        data = {}
        response = self.client.post(url_2, data)
        self.assertEqual(response.status_code, 404)

        # no data: 400
        data = {}
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 400)


class BasicTest(LocalTestCase):
    fixtures = ['sd_store_data', 'auth_users', 'sd_store_users']

    def setUp(self):
        TestCase.setUp(self)
        #
        self.bscURLs = ['/sensors/', '/referenceConsumption/']
        '''
        self.argURLs = ['/energy/data/', '/power/data/', '/energy/alwaysOn/',
                        '/power/alwaysOn/', '/energy/total/',
                        '/energy/totalCost/',
                        '/liveStats/', '/savings/']
        '''
        # self.extURLs = ['/powerNow/', '/getEventLog/', '/smartPlugState/']
        # self.extURLs = ['/powerNow/', ]
        '''
        self.actURLs = ['/toggleSmartPlug/', '/smartPlugOn/', '/smartPlugOff/',
                        '/buttonFlashOn/', '/buttonFlashOff/',
                        '/toggleUserBattery/']
        '''

        self.username = 'user3@example.com'
        self.password = 'test'

    def test_admin(self):
        self.client.login(username='superuser', password='test')
        response = self.client.get('/admin/', follow=False)
        self.assertEqual(response.status_code, 200)

    def test_authentication(self):
        # without authentication we should get a forbidden access error for any view

        for u in self.bscURLs:  # + self.argURLs + self.extURLs
            response = self.client.get(u, follow=False)
            self.assertEqual(response.status_code, 302)
            # TODO: check the redirection
            # print response.redirect_chain
            # self.assertRedirects(response, '/accounts/login/?next='+u)

    def test_login(self):
        # post good username and password: 200
        response = self.client.post('/login/',
                                    {'username': self.username, 'password': self.password})
        self.assertEqual(response.status_code, 200)
        # post wrong password: 400
        response = self.client.post('/login/', {'username': self.username, 'password': ''})
        self.assertEqual(response.status_code, 400)
        # post wrong username: 400
        response = self.client.post('/login/', {'username': '', 'password': self.password})
        self.assertEqual(response.status_code, 400)
        # do get request: 400
        response = self.client.get('/login/',
                                   {'username': self.username, 'password': self.password})
        self.assertEqual(response.status_code, 400)

    def test_logout(self):
        # post good username and password: 200
        response = self.client.post('/login/',
                                    {'username': self.username, 'password': self.password})
        self.assertEqual(response.status_code, 200)
        # check that there is access to user sensors
        all_items_response = self.client.get('/sensors/')
        self.assertEqual(all_items_response.status_code, 200)
        # logout
        self.client.post('/logout/')
        all_items_response = self.client.get('/sensors/')
        self.assertEqual(all_items_response.status_code, 302)

    def test_user(self):
        # login with no staff user
        self.client.login(username=self.username, password=self.password)
        response = self.client.get('/users/')
        self.assertEqual(response.status_code, 403)
        self.client.logout()
        # login with a staff user
        user_name = User.objects.filter(is_staff=True)[0].username
        self.client.login(username=user_name, password='test')
        response = self.client.get('/users/')
        self.assertEqual(response.status_code, 200)

    def test_sensors(self):
        self.client.login(username=self.username, password=self.password)

        all_items_response = self.client.get('/sensors/')
        self.assertEqual(all_items_response.status_code, 200)

        parsed = simplejson.loads(str(all_items_response.content))
        self.assertEqual(len(parsed), 6)

        sensor_response = self.client.get('/sensor/%d/' % (parsed[0]['id']))
        self.assertEqual(sensor_response.status_code, 200)

        json_response = simplejson.loads(str(sensor_response.content))
        self.assertEqual(json_response['mac'], '00-0D-6F-00-00-1D-67-BE')

    def test_sensors_access(self):
        # test that user3 cannot access a sensor they do not own
        self.client.login(username=self.username, password=self.password)

        other_sensors = Sensor.objects.exclude(user=User.objects.get(username=self.username))
        self.assertGreater(len(other_sensors), 0,
                           "Needs at least one sensor that %s can't access." % (self.username,))
        for sensor in other_sensors:
            sensor_response = self.client.get('/sensor/%d/' % sensor.id)
            self.assertEqual(sensor_response.status_code, 403)

    def test_sensor_unauthenticated(self):
        # Unauthenticated user tried to use this call: 302 (Should be 403, but never mind!)
        sensor_response = self.client.get("/sensors/")
        self.assertEqual(sensor_response.status_code, 302)
        sensor_response = self.client.get('/sensor/0/')
        self.assertEqual(sensor_response.status_code, 302)

    def test_sensor_unsupported(self):
        # Login
        self.client.login(username=self.username, password=self.password)

        # User uses unsupported methods: 400
        sensor_response = self.client.put("/sensors/")
        self.assertEqual(sensor_response.status_code, 400)
        sensor_response = self.client.put("/sensor/0/")
        self.assertEqual(sensor_response.status_code, 400)

    def test_sensor_GET(self):
        # Get relevant data from the DB
        user = User.objects.get(username=self.username)
        sensors = Sensor.objects.filter(user=user)
        other_sensors = Sensor.objects.exclude(user=user)
        self.assertGreater(len(sensors), 0)
        self.assertGreater(len(other_sensors), 0)

        # Login
        self.client.login(username=self.username, password=self.password)

        # User GETs /sensors/ : 200
        sensor_response = self.client.get('/sensors/')
        response_content = json.loads(sensor_response.content)
        response_macs = [s['mac'] for s in response_content]
        for s in sensors:
            self.assertTrue(s.mac in response_macs)
        self.assertEqual(sensor_response.status_code, 200)

        # User GETs his /sensor/X/ : 200
        sensor_response = self.client.get("/sensor/%d/" % (sensors[0].id,))
        self.assertEqual(sensor_response.status_code, 200)

        # User GETs his /sensor/X/ : 200 via mac address
        sensor_response = self.client.get("/sensor/%s/" % (sensors[0].mac,))
        self.assertEqual(sensor_response.status_code, 200)

        # User GETs another user's /sensor/X/: 403
        sensor_response = self.client.get('/sensor/%d/' % (other_sensors[0].id,))
        self.assertEqual(sensor_response.status_code, 403)
        # User GETs a non-existing /sensor/X/: 404
        # Sorry this is a tad evil.
        # It basically finds the first integer that doesn't have a matching sensor with that PK
        x = 0
        while x in [sensor.id for sensor in sensors or other_sensors]:
            x += 1
        else:  # else for a while loop executes after the condition evaluates False
            # And then runs this test:
            sensor_response = self.client.get('/sensor/%d/' % (x,))
            self.assertEqual(sensor_response.status_code, 404)

    def test_sensor_POST(self):
        # Get relevant data from the DB
        user = User.objects.get(username=self.username)
        sensors = Sensor.objects.filter(user=user)
        other_sensors = Sensor.objects.exclude(user=user)
        self.assertGreater(len(sensors), 0)
        self.assertGreater(len(other_sensors), 0)

        test_sensor = {
            "mac": "FF-FF-FF-FF-FF-FF-FF-FF",
            'name': 'My Test Meter',
            'sensor_type': 'MeterReader'
        }

        test_existing_update = {
            "mac": "00-00-00-00-00-00-00-00",
            'name': 'My updated Test Meter',
            'sensor_type': 'UpdatedReader'
        }

        invalid_params = {
            'mac': 'FF-FF-FF-FF-FF-FF-FF-FE',
            'name': "My Broken Test Meter"
        }

        # Login
        self.client.login(username=self.username, password=self.password)

        # User POSTs to /sensors/ with new sensor: 201
        sensor_response = self.client.post('/sensors/', test_sensor)
        self.assertEqual(sensor_response.status_code, 201)
        # User POSTs to /sensors/ with invalid params: 400
        sensor_response = self.client.post('/sensors/', invalid_params)
        self.assertEqual(sensor_response.status_code, 400)
        # User POSTs to /sensor/X/ with existing sensor to update: 200
        sensor_response = self.client.post('/sensor/%d/' % (sensors[0].id,), test_existing_update)
        self.assertEqual(sensor_response.status_code, 200)
        # User POSTs to /sensor/X/ with invalid params: 400
        sensor_response = self.client.post('/sensor/%d/' % (sensors[0].id,), invalid_params)
        self.assertEqual(sensor_response.status_code, 400)
        # User POSTs to /sensor/X/ with another user's X: 403
        sensor_response = self.client.post('/sensor/%d/' % (other_sensors[0].id,), test_sensor)
        self.assertEqual(sensor_response.status_code, 403)
        # User POSTs to /sensor/X/ with nonexistent X: 404
        x = 0
        while x in [sensor.id for sensor in sensors or other_sensors]:
            x += 1
        else:  # else for a while loop executes after the condition evaluates False
            # And then runs this test:
            sensor_response = self.client.post('/sensor/%d/' % (x,), test_sensor)
            self.assertEqual(sensor_response.status_code, 404)

    def test_sensor_DELETE(self):
        # Get relevant data from the DB
        user = User.objects.get(username=self.username)
        sensors = Sensor.objects.filter(user=user)
        count = Sensor.objects.count()
        other_sensors = Sensor.objects.exclude(user=user)

        self.assertGreater(len(sensors), 0)
        self.assertGreater(len(other_sensors), 0)
        # Login
        self.client.login(username=self.username, password=self.password)

        # User DELETE to /sensors/ : 403 <== This can be changed later
        sensor_response = self.client.delete('/sensors/')
        self.assertEqual(sensor_response.status_code, 403)
        # User DELETE to /sensor/X/ with his X: 200
        sensor_response = self.client.delete('/sensor/%d/' % sensors[0].id)
        self.assertEqual(sensor_response.status_code, 200)
        self.assertEqual(Sensor.objects.count(), count - 1)
        # User DELETE to /sensor/X/ with another user's X: 403
        sensor_response = self.client.delete('/sensor/%d/' % other_sensors[0].id)
        self.assertEqual(sensor_response.status_code, 403)
        self.assertEqual(Sensor.objects.count(), count - 1)
        # User DELETE to /sensor/X/ with nonexistent X: 404
        x = 0
        while x in [sensor.id for sensor in sensors or other_sensors]:
            x += 1
        else:  # else for a while loop executes after the condition evaluates False
            # And then runs this test:
            sensor_response = self.client.delete('/sensor/%d/' % (x,))
            self.assertEqual(sensor_response.status_code, 404)
            self.assertEqual(Sensor.objects.count(), count - 1)

    def test_channel_unauthenticated(self):
        user = User.objects.get(username=self.username)
        sensor = Sensor.objects.filter(user=user, sensor_type="MeterReader")[0]
        channel_response = self.client.get('/sensor/%d/%s/' % (sensor.id, 'energy'))
        self.assertEqual(channel_response.status_code, 302)

    def test_channel_unsupported(self):
        user = User.objects.get(username=self.username)
        sensor = Sensor.objects.filter(user=user, sensor_type="MeterReader")[0]

        # Login
        self.client.login(username=self.username, password=self.password)

        channel_response = self.client.put('/sensor/%d/%s/' % (sensor.id, 'energy'))
        self.assertEqual(channel_response.status_code, 400)

    def test_channel_GET(self):
        user = User.objects.get(username=self.username)
        sensor = Sensor.objects.filter(user=user, sensor_type="MeterReader")[0]
        button = Sensor.objects.filter(user=user, sensor_type="Button")[0]
        other_sensor = Sensor.objects.exclude(user=user).filter(sensor_type="MeterReader")[0]
        all_sensor_ids = [x.id for x in Sensor.objects.all()]

        # Login
        self.client.login(username=self.username, password=self.password)

        # Begin tests for GET calls
        # User GETs correct channel: 200
        channel_response = self.client.get('/sensor/%d/%s/' % (sensor.id, 'energy'))
        self.assertEqual(channel_response.status_code, 200)
        # User GETs channel for another user: 403
        channel_response = self.client.get('/sensor/%d/%s/' % (other_sensor.id, 'energy'))
        self.assertEqual(channel_response.status_code, 403)
        # User GETs valid sensor with incorrect channel: 404
        channel_response = self.client.get('/sensor/%d/%s/' % (sensor.id, 'foo'))
        self.assertEqual(channel_response.status_code, 404)
        # User GETs valid sensor with valid, but unconnected channel: 404
        channel_response = self.client.get('/sensor/%d/%s/' % (button.id, 'energy'))
        self.assertEqual(channel_response.status_code, 404)
        # User GETs incorrect sensor: 404
        x = 0
        while x in all_sensor_ids:
            x += 1
        else:
            channel_response = self.client.get('/sensor/%d/%s/' % (x, 'energy'))
            self.assertEqual(channel_response.status_code, 404)

    def test_channel_POST(self):
        user = User.objects.get(username=self.username)
        sensor = Sensor.objects.filter(user=user, sensor_type="MeterReader")[0]
        other_sensor = Sensor.objects.exclude(user=user).filter(sensor_type="MeterReader")[0]
        all_sensor_ids = [x.id for x in Sensor.objects.all()]
        sensor_channel_count = sensor.channels.count()
        other_sensor_channel_count = other_sensor.channels.count()
        global_channel_count = Channel.objects.count()

        sound_channel = {"unit": "dB", "reading_frequency": 1}

        # Nonsense, malformed parameters.
        fish_channel = {'unit': 'cod', 'reading_frequency': 'haddock'}

        # Login
        self.client.login(username=self.username, password=self.password)

        # Begin Tests
        # User POSTs a channel with incorrect parameters: 400
        channel_response = self.client.post('/sensor/%d/%s/' % (sensor.id, 'energy'))
        self.assertEqual(channel_response.status_code, 400)
        # User POSTs a channel with malformed parameters: 400
        channel_response = self.client.post('/sensor/%d/%s/' % (sensor.id, 'fish'), fish_channel)
        self.assertEqual(channel_response.status_code, 400)
        # User POSTs a new channel with correct parameters: 201
        channel_response = self.client.post('/sensor/%d/%s/' % (sensor.id, 'volume'),
                                            sound_channel)
        self.assertEqual(channel_response.status_code, 201)
        self.assertEqual(sensor.channels.count(), sensor_channel_count + 1)
        # User POSTs an existing channel with correct parameters: 200
        channel_response = self.client.post('/sensor/%d/%s/' % (sensor.id, 'volume'),
                                            sound_channel)
        self.assertEqual(channel_response.status_code, 200)
        self.assertEqual(sensor.channels.count(), sensor_channel_count + 1)
        # User POSTs a channel to another user's sensor: 403
        channel_response = self.client.post('/sensor/%d/%s/' % (other_sensor.id, 'volume'),
                                            sound_channel)
        self.assertEqual(channel_response.status_code, 403)
        self.assertEqual(other_sensor.channels.count(), other_sensor_channel_count)
        # User POSTs a channel to a non-existent sensor: 404
        x = 0
        while x in all_sensor_ids:
            x += 1
        else:
            channel_response = self.client.post('/sensor/%d/%s/' % (x, 'volume'), sound_channel)
            self.assertEqual(channel_response.status_code, 404)

        # Verify that only one additional global channel has been created
        self.assertEqual(Channel.objects.count(), global_channel_count + 1)

    def test_channel_DELETE(self):
        user = User.objects.get(username=self.username)
        sensor = Sensor.objects.filter(user=user, sensor_type="MeterReader")[0]
        other_sensor = Sensor.objects.exclude(user=user).filter(sensor_type="MeterReader")[0]
        all_sensor_ids = [x.id for x in Sensor.objects.all()]
        sensor_channel_count = sensor.channels.count()
        other_sensor_channel_count = other_sensor.channels.count()
        global_channel_count = Channel.objects.count()

        # Login
        self.client.login(username=self.username, password=self.password)

        # Begin Tests
        # User DELETE an existing channel: 200
        channel_response = self.client.delete('/sensor/%d/%s/' % (sensor.id, 'energy'))
        self.assertEqual(channel_response.status_code, 200)
        self.assertEqual(sensor.channels.count(), sensor_channel_count - 1)
        # User DELETE another user's existing channel: 403
        channel_response = self.client.delete('/sensor/%d/%s/' % (other_sensor.id, 'energy'))
        self.assertEqual(channel_response.status_code, 403)
        self.assertEqual(other_sensor.channels.count(), other_sensor_channel_count)
        # User DELETE a real sensor's non-existent channel: 404
        channel_response = self.client.delete('/sensor/%d/%s/' % (sensor.id, 'foo'))
        self.assertEqual(channel_response.status_code, 404)
        self.assertEqual(sensor.channels.count(), sensor_channel_count - 1)
        # User DELETE a non-existent sensor's channel: 404
        x = 0
        while x in all_sensor_ids:
            x += 1
        else:
            channel_response = self.client.delete('/sensor/%d/%s/' % (x, 'energy'))
            self.assertEqual(channel_response.status_code, 404)

        # Verify that none of the global channels have been deleted
        self.assertEqual(Channel.objects.count(), global_channel_count)

    def test_data_unauthenticated(self):
        # Unauthenticated user tried to use this call: 302 (Should be 403, but never mind!)
        data_response = self.client.post("/sensor/0/energy/data/", {})
        self.assertEqual(data_response.status_code, 302)

    def test_data_unsupported(self):
        user = User.objects.get(username=self.username)
        sensor = Sensor.objects.filter(user=user, sensor_type="MeterReader")[0]

        # Login
        self.client.login(username=self.username, password=self.password)

        # PUT should fail
        data_response = self.client.put('/sensor/%d/%s/data/' % (sensor.id, 'energy'))
        self.assertEqual(data_response.status_code, 405)

    @nottest
    def test_data_GET(self):
        user = User.objects.get(username=self.username)
        other_sensors = Sensor.objects.exclude(user=user)
        sensor = Sensor.objects.filter(user=user, sensor_type="MeterReader")[0]
        sensor.save()
        channel = sensor.channels.get(name="energy")
        channel.save()
        sensor.channels.add(channel)
        sensor.save()
        value = 0.1
        time = datetime(2012, 12, 6, 18, 0)

        r = SensorReading(timestamp=time,
                          value=value,
                          sensor=sensor,
                          channel=channel)
        r.save()

        interval = {'sampling_interval': 120, 'start': '2000-06-10 00:00:00',
                    'end': '2013-06-10 00:00:00'}

        # Login
        self.client.login(username=self.username, password=self.password)

        # get request without interval: 400
        data_response = self.client.get('/sensor/%d/%s/data/' % (sensor.id, 'energy'))
        self.assertEqual(data_response.status_code, 400)

        # get request with a wrong channel for the requested sensor: 404
        data_response = self.client.get('/sensor/%d/%s/data/' % (sensor.id, 'wrong'))
        self.assertEqual(data_response.status_code, 404)

        # get data from sensor of another user: 403
        data_response = self.client.get(
            '/sensor/%d/%s/data/' % (other_sensors[0].id, other_sensors[0].channels.all()[0].name))
        self.assertEqual(data_response.status_code, 403)

        # get request with interval: 200
        data_response = self.client.get('/sensor/%d/%s/data/' % (sensor.id, 'energy'), interval)
        self.assertEqual(data_response.status_code, 200)

    def test_data_POST(self):
        user = User.objects.get(username=self.username)
        sensor = Sensor.objects.filter(user=user, sensor_type="MeterReader")[0]
        channel = sensor.channels.get(name="energy")
        other_sensor = Sensor.objects.exclude(user=user).filter(sensor_type="MeterReader")[0]
        all_sensor_ids = [x.id for x in Sensor.objects.all()]

        # Login
        self.client.login(username=self.username, password=self.password)

        # Begin Test
        # With no data: 400
        data_response = self.client.post('/sensor/%d/%s/data/' % (sensor.id, 'energy'))
        self.assertEqual(data_response.status_code, 400)
        # With invalid data: 400
        bad_reading_string = {"data": simplejson.dumps("haddock")}
        data_response = self.client.post('/sensor/%d/%s/data/' % (sensor.id, 'energy'),
                                         bad_reading_string)
        self.assertEqual(data_response.status_code, 400)
        # With correct data: 200

        readings = [{"timestamp": "Wed Aug 01 12:00:00 2012",
                     "value": 0.1},
                    {"timestamp": "Wed Aug 01 12:05:00 2012",
                     "value": 0.2}]
        reading_string = {"data": simplejson.dumps(readings)}

        wrong_readings = [{"timestamp": "2012-08-01 12:00:00", "wrong": 0.1}]

        wrong_readings_string = {"data": simplejson.dumps(wrong_readings)}

        # With wrong keys: 400
        data_response = self.client.post('/sensor/%d/%s/data/' % (sensor.id, 'energy'),
                                         wrong_readings_string)
        self.assertEqual(data_response.status_code, 400)

        data_response = self.client.post('/sensor/%d/%s/data/' % (sensor.id, 'energy'),
                                         reading_string)
        self.assertEqual(data_response.status_code, 200)
        self.assertEqual(data_response.content, str(len(readings)))

        readings = [{"timestamp": "2012-08-01 12:00:00",
                     "value": 0.1},
                    {"timestamp": "2012-08-01 12:05:00",
                     "value": 0.2}]

        for reading in readings:
            from_db = SensorReading.objects.get(sensor=sensor, channel=channel,
                                                timestamp=datetime.strptime(reading['timestamp'],
                                                                            JS_FMT))
            self.assertEqual(from_db.value, reading['value'])
        # To another user's sensor: 403
        data_response = self.client.post('/sensor/%d/%s/data/' % (other_sensor.id, 'energy'),
                                         reading_string)
        self.assertEqual(data_response.status_code, 403)
        # To a valid sensor and incorrect channel: 404
        data_response = self.client.post('/sensor/%d/%s/data/' % (sensor.id, 'fish'),
                                         reading_string)
        self.assertEqual(data_response.status_code, 404)
        # To an invalid sensor: 404
        x = 0
        while x in all_sensor_ids:
            x += 1
        else:
            tmp_url = '/sensor/%d/%s/data/' % (x, 'energy')
            data_response = self.client.post(tmp_url, reading_string)
            self.assertEqual(data_response.status_code, 404)

    def test_data_POST_mac(self):
        user = User.objects.get(username=self.username)
        sensor = Sensor.objects.filter(user=user, sensor_type="MeterReader")[0]
        channel = sensor.channels.get(name="energy")
        other_sensor = Sensor.objects.exclude(user=user).filter(sensor_type="MeterReader")[0]

        # Login
        self.client.login(username=self.username, password=self.password)

        # Begin Test
        # With no data: 400
        data_response = self.client.post('/sensor/%s/%s/data/' % (sensor.mac, 'energy'))
        self.assertEqual(data_response.status_code, 400)
        # With invalid data: 400
        bad_reading_string = {"data": simplejson.dumps("haddock")}
        data_response = self.client.post('/sensor/%s/%s/data/' % (sensor.mac, 'energy'),
                                         bad_reading_string)
        self.assertEqual(data_response.status_code, 400)
        # With correct data: 200

        readings = [{"timestamp": "Wed Aug 01 12:00:00 2012",
                     "value": 0.1},
                    {"timestamp": "Wed Aug 01 12:05:00 2012",
                     "value": 0.2}]
        reading_string = {"data": simplejson.dumps(readings)}

        data_response = self.client.post('/sensor/%s/%s/data/' % (sensor.mac, 'energy'),
                                         reading_string)
        self.assertEqual(data_response.status_code, 200)
        self.assertEqual(data_response.content, str(len(readings)))

        readings = [{"timestamp": "2012-08-01 12:00:00",
                     "value": 0.1},
                    {"timestamp": "2012-08-01 12:05:00",
                     "value": 0.2}]

        for reading in readings:
            from_db = SensorReading.objects.get(sensor=sensor, channel=channel,
                                                timestamp=datetime.strptime(reading['timestamp'],
                                                                            JS_FMT))
            self.assertEqual(from_db.value, reading['value'])
        # To another user's sensor: 403
        data_response = self.client.post('/sensor/%s/%s/data/' % (other_sensor.mac, 'energy'),
                                         reading_string)
        self.assertEqual(data_response.status_code, 403)
        # To a valid sensor and incorrect channel: 404
        data_response = self.client.post('/sensor/%s/%s/data/' % (sensor.mac, 'fish'),
                                         reading_string)
        self.assertEqual(data_response.status_code, 404)

    def test_data_DELETE(self):
        user = User.objects.get(username=self.username)
        sensor = Sensor.objects.filter(user=user, sensor_type="MeterReader")[0]

        # Login
        self.client.login(username=self.username, password=self.password)

        # Should, at present 501
        data_response = self.client.delete('/sensor/%d/%s/data/' % (sensor.id, 'energy'))
        self.assertEqual(data_response.status_code, 405)

    def test_batch_data(self):
        user = User.objects.get(username=self.username)
        sensor = Sensor.objects.filter(user=user, sensor_type="MeterReader")[0]
        channel = sensor.channels.get(name="energy")
        other_sensor = Sensor.objects.exclude(user=user).filter(sensor_type="MeterReader")[0]

        # Login
        self.client.login(username=self.username, password=self.password)

        # Begin Test
        # With no data: 400
        data_response = self.client.post('/sensor/data/')
        self.assertEqual(data_response.status_code, 400)

        # With invalid data: 400
        bad_reading_string = {"data": simplejson.dumps("haddock")}
        data_response = self.client.post('/sensor/data/', bad_reading_string)
        self.assertEqual(data_response.status_code, 400)

        readings = [{"timestamp": "Wed Aug 01 12:00:00 2012",
                     "value": 0.1,
                     "channel": channel.name,
                     "sensor": sensor.mac},
                    {"timestamp": "Wed Aug 01 12:05:00 2012",
                     "value": 0.2,
                     "channel": channel.name,
                     "sensor": sensor.mac}
                    ]

        reading_string = {"data": simplejson.dumps(readings)}

        reading_string_2 = {"data": simplejson.dumps(readings), 'fix_gaps': 'True'}

        wrong_readings = [{"timestamp": "Wed Aug 01 12:00:00 2012", "wrong": 0.1}]

        wrong_readings_string = {"data": simplejson.dumps(wrong_readings)}

        no_list_readings = {"data": simplejson.dumps({})}

        # No list: 400
        data_response = self.client.post('/sensor/data/', no_list_readings)
        self.assertEqual(data_response.status_code, 400)

        # With wrong keys: 400
        data_response = self.client.post('/sensor/data/', wrong_readings_string)
        self.assertEqual(data_response.status_code, 400)

        # With correct data: 200
        data_response = self.client.post('/sensor/data/', reading_string)
        self.assertEqual(data_response.status_code, 200)
        self.assertEqual(data_response.content, str(len(readings)))

        readings = [{"timestamp": "2012-08-01 12:00:00",
                     "value": 0.1},
                    {"timestamp": "2012-08-01 12:05:00",
                     "value": 0.2}]

        for reading in readings:
            from_db = SensorReading.objects.get(sensor=sensor, channel=channel,
                                                timestamp=datetime.strptime(reading['timestamp'],
                                                                            JS_FMT))
            self.assertEqual(from_db.value, reading['value'])

        # With correct data again: 200
        data_response = self.client.post('/sensor/data/', reading_string)
        self.assertEqual(data_response.status_code, 200)

        # With correct data and fix gaps: 200
        data_response = self.client.post('/sensor/data/', reading_string_2)
        self.assertEqual(data_response.status_code, 200)

        # To another user's sensor: 403
        readings = [{"timestamp": "Wed Aug 01 12:00:00 2012",
                     "value": 0.1,
                     "channel": channel.name,
                     "sensor": other_sensor.mac}
                    ]

        reading_string = {"data": simplejson.dumps(readings)}

        data_response = self.client.post('/sensor/data/', reading_string)
        self.assertEqual(data_response.status_code, 403)

        # To a valid sensor and incorrect channel: 404
        no_channel_in_sensor = [{"timestamp": "Wed Aug 01 12:05:00 2012",
                                 "value": 0.2,
                                 "channel": '',
                                 "sensor": sensor.mac}
                                ]

        reading_string = {"data": simplejson.dumps(no_channel_in_sensor)}
        data_response = self.client.post('/sensor/data/', reading_string)
        self.assertEqual(data_response.status_code, 404)

        # To an invalid sensor: 404
        wrong_sensor = [{"timestamp": "Wed Aug 01 12:05:00 2012",
                         "value": 0.2,
                         "channel": channel.name,
                         "sensor": ''}]

        reading_string = {"data": simplejson.dumps(wrong_sensor)}
        tmp_url = '/sensor/data/'
        data_response = self.client.post(tmp_url, reading_string)
        self.assertEqual(data_response.status_code, 404)

    def test_snapshot(self):
        # Test unauthenticated: 403
        reading_response = self.client.get('/snapshot/')
        self.assertEqual(reading_response.status_code, 403)

        # Login
        self.client.login(username=self.username, password=self.password)

        # Test user: 200
        reading_response = self.client.get('/snapshot/')
        self.assertEqual(reading_response.status_code, 200)

    def test_last_reading_view(self):
        user = User.objects.get(username=self.username)
        sensor = Sensor.objects.filter(user=user, sensor_type="MeterReader")[0]
        other_sensor = Sensor.objects.exclude(user=user).filter(sensor_type="MeterReader")[0]
        all_sensor_ids = [x.id for x in Sensor.objects.all()]

        # Test unauthenticated
        reading_response = self.client.get('/sensor/%d/%s/last-reading/' % (sensor.id, 'energy'))
        self.assertEqual(reading_response.status_code, 302)

        # Login
        self.client.login(username=self.username, password=self.password)

        # Begin Tests
        # Method not GET: 400
        reading_response = self.client.post('/sensor/%d/%s/last-reading/' % (sensor.id, 'energy'))
        self.assertEqual(reading_response.status_code, 405)
        # User GETs correctly: 200
        reading_response = self.client.get('/sensor/%d/%s/last-reading/' % (sensor.id, 'energy'))
        self.assertEqual(reading_response.status_code, 200)
        # User GETs for another user's sensor: 403
        reading_response = self.client.get(
            '/sensor/%d/%s/last-reading/' % (other_sensor.id, 'energy'))
        self.assertEqual(reading_response.status_code, 403)
        # User GETs non-existent channel of existent sensor: 404
        reading_response = self.client.get('/sensor/%d/%s/last-reading/' % (sensor.id, 'fish'))
        self.assertEqual(reading_response.status_code, 404)
        # User GETs non-existent sensor: 404
        x = 0
        while x in all_sensor_ids:
            x += 1
        else:
            reading_response = self.client.get('/sensor/%d/%s/last-reading/' % (x, 'energy'))
            self.assertEqual(reading_response.status_code, 404)

    def test_sensorGroup(self):
        self.client.login(username=self.username, password=self.password)

        all_items_response = self.client.get('/sensorGroups/')
        self.assertEqual(all_items_response.status_code, 200)

        parsed = simplejson.loads(str(all_items_response.content))
        self.assertEqual(len(parsed), 0)

        # create sensor group
        created_response = self.client.post('/sensorGroups/', {
            'name': '1stFloor',
            'description': 'all sensors on the 1st floor'
        })
        self.assertEqual(created_response.status_code, 200)

        all_items_response = self.client.get('/sensorGroups/')
        self.assertEqual(all_items_response.status_code, 200)
        parsed = simplejson.loads(str(all_items_response.content))
        self.assertEqual(len(parsed), 1)
        group_id = parsed[0]['id']

        #  get the first available sensor:
        sensors_response = self.client.get('/sensors/')
        parsed = simplejson.loads(str(sensors_response.content))
        sensor_id = parsed[0]['id']

        # check non get or post request
        added_response = self.client.put('/sensorGroup/%d/sensors/' % group_id)
        self.assertEqual(added_response.status_code, 405)

        # add some sensors to group
        added_response = self.client.post('/sensorGroup/%d/sensors/' % (group_id,), {
            'sensorID': sensor_id
        })
        self.assertEqual(added_response.status_code, 200)

        # check the group contains 1 sensor, the one we just added
        group_sensors_response = self.client.get('/sensorGroup/%d/sensors/' % (group_id,))
        self.assertEqual(group_sensors_response.status_code, 200)
        parsed = simplejson.loads(str(group_sensors_response.content))
        self.assertEqual(len(parsed), 1)
        self.assertEqual(parsed[0]['id'], sensor_id)

        # add readings to sensor of group
        sensor = Sensor.objects.filter(id=sensor_id)[0]
        channel = sensor.channels.all()[0]
        test_reading = SensorReading(timestamp=datetime(2012, 8, 1, 12), channel=channel,
                                     sensor=sensor, value=1)
        test_reading.save()
        test_reading = SensorReading(timestamp=datetime(2012, 8, 1, 12, 2), channel=channel,
                                     sensor=sensor, value=2)
        test_reading.save()
        test_reading = SensorReading(timestamp=datetime(2012, 8, 1, 12, 4), channel=channel,
                                     sensor=sensor, value=3)
        test_reading.save()

        # check that created data is returned
        interval = {'start': '2000-06-10 00:00:00', 'end': '2015-06-10 00:00:00',
                    'sampling_interval': 120}
        group_data_response = self.client.get('/sensorGroup/%d/data/' % group_id, interval)
        self.assertEqual(group_data_response.status_code, 200)
        parsed = simplejson.loads(str(group_data_response.content))
        self.assertEqual(len(parsed), 1)
        self.assertEqual(parsed[0]['channels_data'][0]['min_datetime'], '2012-08-01 12:00:00')
        self.assertEqual(parsed[0]['channels_data'][0]['max_datetime'], '2012-08-01 12:04:00')

        # delete sensor from group
        added_response = self.client.delete('/sensorGroup/%d/sensor/%d/' % (group_id, sensor_id))
        self.assertEqual(added_response.status_code, 200)

        # check the group contains 0 sensors
        group_sensors_response = self.client.get('/sensorGroup/%d/sensors/' % (group_id,))
        self.assertEqual(group_sensors_response.status_code, 200)
        parsed = simplejson.loads(str(group_sensors_response.content))
        self.assertEqual(len(parsed), 0)

        # trying to delete the sensor from the group again should fail
        added_response = self.client.delete('/sensorGroup/%d/sensor/%d/' % (group_id, sensor_id))
        self.assertEqual(added_response.status_code, 404)

        # delete sensor group
        delete_response = self.client.delete('/sensorGroup/%d/' % group_id)
        self.assertEqual(delete_response.status_code, 200)
        get_response = self.client.get('/sensorGroup/%d/' % group_id)
        self.assertEqual(get_response.status_code, 404)

    def test_referenceConsumption(self):
        self.client.login(username=self.username, password=self.password)

        response = self.client.get('/referenceConsumption/')
        self.assertEqual(response.status_code, 200)

        consumption = simplejson.loads(str(response.content))
        self.assertEqual(consumption['baseline_consumption'], 4.0)

        # TODO: implement the following
        # def test_live_stats(self):
        #   raise NotImplementedError

        # TODO: implement the following
        # def test_savings(self):
        #   raise NotImplementedError


def energy_f(x):
    return 0.2 + 1.0 + sin(2 * pi * x / 60.0)


def generate_energy_data(user, current_dt):
    profile = UserProfile.objects.get(user=user)
    meter_one = profile.primary_sensor
    channel = filter(lambda x: x.name == 'energy', meter_one.channels.all())[0]

    data_days = 10
    s = current_dt - timedelta(days=data_days)

    from itertools import cycle
    # create sinusoidal data
    hour_data = [energy_f(x) for x in range(0, 60, 2)]
    data_source = cycle(hour_data)
    all_values = []
    for t in (s + timedelta(minutes=m) for m in range(0, 60 * 24 * data_days, 2)):
        v = data_source.next()
        data_point = SensorReading(timestamp=t, sensor=meter_one, channel=channel, value=v)
        data_point.save()
        all_values.append(v)

    from sd_store.sdutils.store_utils import ALWAYS_ON_WINDOW_SIZE
    smoothed = moving_average(all_values, ALWAYS_ON_WINDOW_SIZE)

    always_on = min(smoothed)
    return always_on


class DataTest(LocalTestCase):
    # fixtures = ['sd_store_data', 'sd_store_users', 'sd_store_energy']
    fixtures = ['sd_store_data', 'auth_users', 'sd_store_users']

    def setUp(self):
        TestCase.setUp(self)

        self.username = 'user3@example.com'
        self.password = 'test'

        self.user = User.objects.get(username=self.username)

        self.now = datetime(2013, 4, 4, 18)

        self.always_on = generate_energy_data(self.user, self.now)

        user_sensors = Sensor.objects.filter(user=self.user)
        self.energy_sensor = filter(lambda x: x.sensor_type == 'MeterReader', user_sensors)[0]
        self.energy_channel = Channel.objects.get(name='energy')

        self.client.login(username=self.username, password=self.password)

    def test_baseline(self):
        url = '/sensor/%d/%s/baseline/' % (self.energy_sensor.id, self.energy_channel.name)

        # with no arguments, it should fail: 404
        response = self.client.get(url)

        self.assertEqual(response.status_code, 400)
        parsed = simplejson.loads(str(response.content))
        self.assertEqual(len(parsed['error']['reason'].keys()), 3)

        # access a sensor not owned by the user: 403
        other_sensor = Sensor.objects.exclude(user=self.user.id)[0]
        url_2 = '/sensor/%d/%s/baseline/' % (other_sensor.id, self.energy_channel.name)
        response = self.client.get(url_2)
        self.assertEqual(response.status_code, 403)

        # pass bad arguments
        start = 'str((self.now - timedelta(days=7)))'
        end = 'str(self.now)'
        sampling_interval = 120
        response = self.client.get(url, {'start': start, 'end': end,
                                         'sampling_interval': sampling_interval})
        self.assertEqual(response.status_code, 400)
        parsed = simplejson.loads(str(response.content))
        self.assertEqual(len(parsed['error']['reason'].keys()), 2)

        # pass correct arguments
        start = self.now - timedelta(days=7)
        end = self.now
        # use JavaScript date format
        start = start.strftime(JS_FMT)
        end = end.strftime(JS_FMT)
        sampling_interval = 120
        response = self.client.get(url, {'start': start, 'end': end,
                                         'sampling_interval': sampling_interval})

        self.assertEqual(response.status_code, 200)
        parsed = simplejson.loads(str(response.content))
        data = parsed['data']
        self.assertEqual(len(data), 7 * 24 * 30)

        min_value = min([x['value'] for x in data])
        self.assertGreaterEqual(min_value, 0.0)

        # start and end invalid
        response = self.client.get(url, {'start': end, 'end': start,
                                         'sampling_interval': sampling_interval})
        self.assertEqual(response.status_code, 400)

        # no readings for provided sensor and channel
        sensor_2 = Sensor.objects.exclude(mac=self.energy_sensor.mac).filter(user=self.user)[0]
        channel_2 = sensor_2.channels.all()[0]
        url_3 = '/sensor/%d/%s/baseline/' % (sensor_2.id, channel_2.name)
        response = self.client.get(url_3, {'start': start, 'end': end,
                                           'sampling_interval': sampling_interval})

        self.assertEqual(response.status_code, 200)
        parsed = simplejson.loads(str(response.content))
        data = parsed['data']
        self.assertEqual(len(data), 0)

    def test_integral(self):
        url = '/sensor/%d/%s/integral/' % (self.energy_sensor.id, self.energy_channel.name)

        # with no arguments, it should fail
        response = self.client.get(url)

        self.assertEqual(response.status_code, 400)
        parsed = simplejson.loads(str(response.content))
        self.assertEqual(len(parsed['error']['reason'].keys()), 2)

        # access a sensor not owned by the user: 403
        other_sensor = Sensor.objects.exclude(user=self.user.id)[0]
        url_2 = '/sensor/%d/%s/integral/' % (other_sensor.id, self.energy_channel.name)
        response = self.client.get(url_2)
        self.assertEqual(response.status_code, 403)

        # pass bad arguments
        start = 'str((self.now - timedelta(days=7)))'
        end = 'str(self.now)'
        response = self.client.get(url, {'start': start, 'end': end})
        self.assertEqual(response.status_code, 400)
        parsed = simplejson.loads(str(response.content))
        self.assertEqual(len(parsed['error']['reason'].keys()), 2)

        # pass correct arguments
        start = self.now - timedelta(days=7)
        end = self.now
        # use JavaScript date format
        start = start.strftime(JS_FMT)
        end = end.strftime(JS_FMT)
        response = self.client.get(url, {'start': start, 'end': end})

        self.assertEqual(response.status_code, 200)
        parsed = simplejson.loads(str(response.content))
        data = parsed['data']
        self.assertEqual(float(int(data)), 6048.0)

        # start and end invalid
        response = self.client.get(url, {'start': end, 'end': start})
        self.assertEqual(response.status_code, 400)

    def test_energy_data(self):
        url = '/sensor/%d/%s/data/' % (self.energy_sensor.id, self.energy_channel.name)

        # with no arguments, it should fail
        response = self.client.get(url)

        self.assertEqual(response.status_code, 400)
        parsed = simplejson.loads(str(response.content))
        self.assertEqual(len(parsed['error']['reason'].keys()), 3)

        # pass bad arguments
        start = 'str((self.now - timedelta(days=7)))'
        end = 'str(self.now)'
        sampling_interval = 120
        response = self.client.get(url, {'start': start, 'end': end,
                                         'sampling_interval': sampling_interval})
        self.assertEqual(response.status_code, 400)
        parsed = simplejson.loads(str(response.content))
        self.assertEqual(len(parsed['error']['reason'].keys()), 2)

        # pass correct arguments
        start = self.now - timedelta(days=7)
        end = self.now
        # use JavaScript date format
        start = start.strftime(JS_FMT)
        end = end.strftime(JS_FMT)
        sampling_interval = 120
        response = self.client.get(url, {'start': start, 'end': end,
                                         'sampling_interval': sampling_interval})

        self.assertEqual(response.status_code, 200)
        parsed = simplejson.loads(str(response.content))
        data = parsed['data']
        self.assertEqual(len(data), 7 * 24 * 30)
        for i, m in enumerate(range(0, 60, 2)):
            self.assertEqual(data[i]['value'], energy_f(m))

        min_value = min([x['value'] for x in data])
        self.assertGreaterEqual(min_value, 0.0)

        # test re-sampling
        start = self.now - timedelta(hours=8)
        end = self.now - timedelta(hours=2)
        # use JavaScript date format
        sampling_interval = 60 * 60
        response = self.client.get(url,
                                   {'start': start.strftime(JS_FMT),
                                    'end': end.strftime(JS_FMT),
                                    'sampling_interval': sampling_interval})

        self.assertEqual(response.status_code, 200)
        parsed = simplejson.loads(str(response.content))
        data = parsed['data']

        min_value = min([x['value'] for x in data])
        self.assertGreaterEqual(min_value, 0.0)

        self.assertEqual(len(data), 6)
        self.assertEqual(len(data), total_seconds(end - start) / sampling_interval)
        # assert the data is constant
        for i in range(1, len(data)):
            self.assertEqual(data[i - 1]['value'], data[i]['value'])
        self.assertAlmostEqual(data[0]['value'], sum([energy_f(m) for m in range(0, 60, 2)]))

    def test_group_data(self):
        self.client.login(username=self.username, password=self.password)

        # create sensor group
        created_response = self.client.post('/sensorGroups/', {
            'name': '1stFloor',
            'description': 'all sensors on the 1st floor'
        })
        self.assertEqual(created_response.status_code, 200)

        # get the groupID
        all_items_response = self.client.get('/sensorGroups/')
        self.assertEqual(all_items_response.status_code, 200)
        parsed = simplejson.loads(str(all_items_response.content))
        group_id = parsed[0]['id']

        #  get the first 2 available sensors:
        sensors_response = self.client.get('/sensors/')
        parsed = simplejson.loads(str(sensors_response.content))
        self.assertGreaterEqual(len(parsed), 2)
        # sensorID =

        # add some sensors to group
        added_response = self.client.post('/sensorGroup/%d/sensors/' % (group_id,), {
            'sensorID': parsed[0]['id']
        })
        self.assertEqual(added_response.status_code, 200)

        added_response = self.client.post('/sensorGroup/%d/sensors/' % (group_id,), {
            'sensorID': parsed[1]['id']
        })
        self.assertEqual(added_response.status_code, 200)

        # get data from the group
        start = self.now - timedelta(days=7)
        end = self.now
        # use JavaScript date format
        start = start.strftime(JS_FMT)
        end = end.strftime(JS_FMT)
        sampling_interval = 120

        # Create readings
        sensor1 = Sensor.objects.get(id=parsed[0]['id'])
        sensor2 = Sensor.objects.get(id=parsed[1]['id'])
        channel1 = Channel.objects.filter(sensor=sensor1)[0]
        channel2 = Channel.objects.filter(sensor=sensor2)[0]
        self.assertEqual(len(SensorReading.objects.filter(sensor=sensor1)), 0)
        self.assertEqual(len(SensorReading.objects.filter(sensor=sensor2)), 0)
        reading = SensorReading(sensor=sensor1, channel=channel1, timestamp=start, value=1.1)
        reading.save()
        reading = SensorReading(sensor=sensor2, channel=channel2, timestamp=start, value=1.1)
        reading.save()
        self.assertGreater(len(SensorReading.objects.filter(sensor=sensor1)), 0)
        self.assertGreater(len(SensorReading.objects.filter(sensor=sensor2)), 0)

        # Get group data
        group_data_response = self.client.get('/sensorGroup/%d/data/' % (group_id,),
                                              {'start': start, 'end': end,
                                               'sampling_interval': sampling_interval})
        self.assertEqual(group_data_response.status_code, 200)
        parsed = simplejson.loads(str(group_data_response.content))
        self.assertEqual(len(parsed), 2)
