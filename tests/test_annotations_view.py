# -*- coding: UTF-8 -*-

# This file is part of sd_store
#
# sd_store is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# sd_store is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with sd_store.  If not, see <http://www.gnu.org/licenses/>.
import json

from django.test import TestCase

from django.contrib.auth.models import User
from django.utils import simplejson

from sd_store.models import Annotation


class LocalTestCase(TestCase):
    urls = 'sd_store.test_urls'


class AnnotationTest(LocalTestCase):
    fixtures = ['sd_store_data', 'sd_store_annotations', 'auth_users']

    def setUp(self):
        TestCase.setUp(self)

        self.username = 'user3@example.com'
        self.password = 'test'
        self.start = '2012-06-10 00:00:00'
        self.end = '2012-07-10 00:00:00'

    def test_annotations(self):
        self.client.login(username=self.username, password=self.password)

        interval = {
            'start': '2000-06-10 00:00:00',
            'end': '2015-07-10 00:00:00'
        }

        all_items_response = self.client.get('/annotations/', interval)
        self.assertEqual(all_items_response.status_code, 200)

        parsed = simplejson.loads(str(all_items_response.content))
        self.assertEqual(len(parsed), 1)

        print 'parsed:', parsed
        annotation_response = self.client.get('/annotation/%d/' % (parsed[0]['id']))
        self.assertEqual(annotation_response.status_code, 200)

        json_response = simplejson.loads(str(annotation_response.content))
        print json_response
        self.assertEqual(json_response['id'], 2)

    def test_annotations_access(self):
        # test that user3  cannot access an annotation they do not own
        self.client.login(username=self.username, password=self.password)

        other_annotation = Annotation.objects.exclude(
            user=User.objects.get(username=self.username))
        self.assertGreater(len(other_annotation), 0,
                           "Needs at least one annotation that %s can't access." % (
                           self.username,))
        for annotation in other_annotation:
            annotation_response = self.client.get('/annotation/%d/' % annotation.id)
            self.assertEqual(annotation_response.status_code, 403)

    def test_annotation_unauthenticated(self):
        # Unauthenticated user tried to use this call: 302 (Should be 403, but never mind!)
        annotation_response = self.client.get("/annotations/")
        self.assertEqual(annotation_response.status_code, 302)
        annotation_response = self.client.get('/annotation/0/')
        self.assertEqual(annotation_response.status_code, 302)

    def test_annotation_unsupported(self):
        # Login
        self.client.login(username=self.username, password=self.password)

        # User uses unsupported methods: 400
        annotation_response = self.client.put("/annotations/")
        self.assertEqual(annotation_response.status_code, 400)
        annotation_response = self.client.put("/annotation/0/")
        self.assertEqual(annotation_response.status_code, 400)

    def test_annotation_GET(self):
        # Get relevant data from the DB
        user = User.objects.get(username=self.username)
        annotations = Annotation.objects.filter(user=user)
        other_annotations = Annotation.objects.exclude(user=user)
        self.assertGreater(len(annotations), 0)
        self.assertGreater(len(other_annotations), 0)

        # Login
        self.client.login(username=self.username, password=self.password)

        # User GETs /annotations/ : 200
        annotation_response = self.client.get('/annotations/',
                                              {'start': self.start, 'end': self.end})
        response_content = json.loads(annotation_response.content)
        response_ids = [a['id'] for a in response_content]
        for a in annotations:
            self.assertTrue(a.id in response_ids)
        self.assertEqual(annotation_response.status_code, 200)

        # User GETs /annotations/ with invalid interval form: 400
        annotation_response = self.client.get('/annotations/', {'start': self.start})
        self.assertEqual(annotation_response.status_code, 400)

        # User GETs /annotations/ incorrect start and end dates: 400
        annotation_response = self.client.get('/annotations/',
                                              {'start': self.end, 'end': self.start})
        self.assertEqual(annotation_response.status_code, 400)

        # User GETs his /annotation/X/ : 200
        annotation_response = self.client.get("/annotation/%d/" % (annotations[0].id,))
        self.assertEqual(annotation_response.status_code, 200)

        # User GETs another user's /annotation/X/: 403
        annotation_response = self.client.get('/annotation/%d/' % (other_annotations[0].id,))
        self.assertEqual(annotation_response.status_code, 403)

        # User GETs a non-existing /annotation/X/: 404
        # Sorry this is a tad evil.
        # It basically finds the first integer that doesn't have a matching annotation with that PK
        x = 0
        while x in [annotation.id for annotation in annotations or other_annotations]:
            x += 1
        else:  # else for a while loop executes after the condition evaluates False
            # And then runs this test:
            annotation_response = self.client.get('/annotation/%d/' % (x,))
            self.assertEqual(annotation_response.status_code, 404)

    def test_annotation_POST(self):
        # Get relevant data from the DB
        user = User.objects.get(username=self.username)
        annotations = Annotation.objects.filter(user=user)
        other_annotations = Annotation.objects.exclude(user=user)
        self.assertGreater(len(annotations), 0)
        self.assertGreater(len(other_annotations), 0)

        test_annotation = {
            'start': '2012-06-10 00:00:00',
            'end': '2012-07-10 00:00:00',
            'text': "",
            'pairs': simplejson.dumps([{'sensor': {'id': 2}, 'channel': {'id': 2}}])
        }

        test_annotation_2 = {
            'start': '2012-01-10 00:00:00',
            'end': '2012-07-10 00:00:00',
            'text': "",
            'pairs': simplejson.dumps([{'sensor': {'id': 2}, 'channel': {'id': 2}}])
        }

        invalid_pair = {
            'start': '2012-06-10 00:00:00',
            'end': '2012-07-10 00:00:00',
            'text': "",
            'pairs': simplejson.dumps([{'sensor': {'id': 99}, 'channel': {'id': 99}}])
        }

        invalid_annotation = {
            'start': '2019-06-10 00:00:00',
            'end': '2012-07-10 00:00:00',
            'text': "",
            'pairs': simplejson.dumps([{'sensor': {'id': 2}, 'channel': {'id': 2}}])
        }

        invalid_params = {
            'start': self.start
        }

        # Login
        self.client.login(username=self.username, password=self.password)

        # User POSTs to /annotations/ with new annotation: 201
        annotation_response = self.client.post('/annotations/', test_annotation)
        self.assertEqual(annotation_response.status_code, 201)
        # User POSTs to /annotations/ with invalid params: 400
        annotation_response = self.client.post('/annotations/', invalid_params)
        self.assertEqual(annotation_response.status_code, 400)
        # User POSTs /annotations/ with invalid pair: 404
        annotation_response = self.client.post('/annotations/', invalid_pair)
        self.assertEqual(annotation_response.status_code, 404)
        # User POSTs /annotations/ with invalid annotation: 400
        annotation_response = self.client.post('/annotations/', invalid_annotation)
        self.assertEqual(annotation_response.status_code, 400)
        # User POSTs to /annotation/X/ with existing id X: 200
        annotation_response = self.client.post('/annotation/%d/' % (annotations[0].id,),
                                               test_annotation_2)
        self.assertEqual(annotation_response.status_code, 200)
        # User POSTs to /annotation/X/ with invalid params: 400
        annotation_response = self.client.post('/annotation/%d/' % (annotations[0].id,),
                                               invalid_params)
        self.assertEqual(annotation_response.status_code, 400)
        # User POSTs to /annotation/X/ with another user's X: 403
        annotation_response = self.client.post('/annotation/%d/' % (other_annotations[0].id,),
                                               test_annotation)
        self.assertEqual(annotation_response.status_code, 403)
        # User POSTs to /annotation/X/ with nonexistent X: 404
        x = 0
        while x in [annotation.id for annotation in annotations or other_annotations]:
            x += 1
        else:  # else for a while loop executes after the condition evaluates False
            # And then runs this test:
            annotation_response = self.client.post('/annotation/%d/' % (x,), test_annotation)
            self.assertEqual(annotation_response.status_code, 404)

    def test_annotation_DELETE(self):
        # Get relevant data from the DB
        user = User.objects.get(username=self.username)
        annotations = Annotation.objects.filter(user=user)
        count = Annotation.objects.count()
        other_annotations = Annotation.objects.exclude(user=user)

        self.assertGreater(len(annotations), 0)
        self.assertGreater(len(other_annotations), 0)
        # Login
        self.client.login(username=self.username, password=self.password)

        # User DELETE to /annotations/ : 403 <== This can be changed later
        annotations_response = self.client.delete('/annotations/')
        self.assertEqual(annotations_response.status_code, 403)
        # User DELETE to /annotation/X/ with his X: 200
        annotations_response = self.client.delete('/annotation/%d/' % annotations[0].id)
        self.assertEqual(annotations_response.status_code, 200)
        self.assertEqual(Annotation.objects.count(), count - 1)
        # User DELETE to /annotation/X/ with another user's X: 403
        annotations_response = self.client.delete('/annotation/%d/' % other_annotations[0].id)
        self.assertEqual(annotations_response.status_code, 403)
        self.assertEqual(Annotation.objects.count(), count - 1)
        # User DELETE to /annotation/X/ with nonexistent X: 404
        x = 0
        while x in [annotation.id for annotation in annotations or other_annotations]:
            x += 1
        else:  # else for a while loop executes after the condition evaluates False
            # And then runs this test:
            annotations_response = self.client.delete('/annotation/%d/' % (x,))
            self.assertEqual(annotations_response.status_code, 404)
            self.assertEqual(Annotation.objects.count(), count - 1)
