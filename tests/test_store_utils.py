# -*- coding: UTF-8 -*-

# This file is part of sd_store
#
# sd_store is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# sd_store is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with sd_store.  If not, see <http://www.gnu.org/licenses/>.
from datetime import datetime, timedelta

from sd_store.sdutils.store_utils import filter_according_to_interval
from sd_store.sdutils.store_utils import filter_according_to_interval_gen
from sd_store.sdutils.store_utils import fix_data_gaps
from django.test import TestCase
from sd_store.models import SensorReading, Sensor


class LocalTestCase(TestCase):
    urls = 'sd_store.test_urls'


class ResamplingTest(LocalTestCase):
    fixtures = ['sd_store_data', 'auth_users', 'sd_store_users', ]

    def setUp(self):
        self.meter = Sensor.objects.filter(name__startswith='Meter')[0]
        self.channel = self.meter.channels.get(name='energy')

    def test_gap_14(self):
        # create samples with a gap

        start = datetime(2013, 3, 1)
        # 30 hours of data
        for minutes in range(0, 60 * 30, 2):
            dt = start + timedelta(minutes=minutes)
            sr = SensorReading(timestamp=dt, value=1.0, sensor=self.meter, channel=self.channel)
            sr.save()

        # 14 hours gap
        restart = start + timedelta(hours=30 + 14)

        # 30 more hours of data
        for minutes in range(0, 60 * 30, 2):
            dt = restart + timedelta(minutes=minutes)
            sr = SensorReading(timestamp=dt, value=1.0, sensor=self.meter, channel=self.channel)
            sr.save()

        # now try to resample
        readings = filter_according_to_interval(self.meter, self.channel,
                                                start, start + timedelta(hours=72),
                                                12 * 60 * 60, 'energy')

        readings = list(readings)
        print [x.timestamp for x in readings]
        self.assertEqual(len(readings), 72 / 12)

        for r in readings:
            print r.timestamp

            # diff_list = [x1.timestamp - x0.timestamp for (x0, x1) in zip(readings[:-1],
            # readings[1:])]
            # self.assertListEqual(diff_list, [timedelta(hours=12) for _ in diff_list])

    def test_gap_26(self):
        print "Begin of test"
        # create samples with a bigger gap
        start = datetime(2013, 3, 1)
        # 30 hours of data
        for minutes in range(0, 60 * 30, 2):
            dt = start + timedelta(minutes=minutes)
            sr = SensorReading(timestamp=dt, value=1.0, sensor=self.meter, channel=self.channel)
            sr.save()
            # print "--",dt

        # 24 hours gap
        restart = start + timedelta(hours=30 + 24)

        # 60 more hours of data
        for minutes in range(0, 60 * 60, 2):
            dt = restart + timedelta(minutes=minutes)
            sr = SensorReading(timestamp=dt, value=1.0, sensor=self.meter, channel=self.channel)
            sr.save()
            # print "--",dt

        # now try to resample 96
        readings = filter_according_to_interval(self.meter, self.channel,
                                                start, start + timedelta(hours=96),
                                                12 * 60 * 60, 'energy')

        readings = list(readings)
        self.assertEqual(len(readings), 96 / 12 - 1)


class GapTest(LocalTestCase):
    fixtures = ['sd_store_data', 'auth_users', 'sd_store_users', ]

    def setUp(self):
        self.meter = Sensor.objects.filter(sensor_type="MeterReader")[0]
        self.ch = self.meter.channels.get(name="energy")
        self.start = datetime(2013, 3, 3)
        self.end = datetime(2013, 3, 5)

    def test_3_days(self):
        for m in range(0, 60 * 24 * 3, 2):
            td = timedelta(minutes=m)
            ts = self.start + td
            sr = SensorReading(timestamp=ts, sensor=self.meter, channel=self.ch, value=1.0)
            sr.save()
        requested_interval = 6 * 60 * 60

        reading_list = filter_according_to_interval_gen(self.meter, self.ch, self.start, self.end,
                                                        requested_interval,
                                                        "energy")

        reading_list = list(reading_list)
        self.assertEqual(len(reading_list), 2 * 24 / 6)

    def test_2_days(self):
        for m in range(0, 60 * 24 * 2 + 2, 2):
            td = timedelta(minutes=m)
            ts = self.start + td
            sr = SensorReading(timestamp=ts, sensor=self.meter, channel=self.ch, value=1.0)
            sr.save()
        requested_interval = 6 * 60 * 60

        reading_list = filter_according_to_interval_gen(self.meter, self.ch, self.start, self.end,
                                                        requested_interval,
                                                        "energy")

        reading_list = list(reading_list)
        self.assertEqual(len(reading_list), 2 * 24 / 6)


class GapFixing(LocalTestCase):
    fixtures = ['sd_store_data', 'auth_users', 'sd_store_users', ]

    def setUp(self):
        self.sensor = Sensor.objects.filter(sensor_type="MeterReader")[0]
        self.channel = self.sensor.channels.get(name='energy')
        self.o_channel = self.sensor.channels.get(name='temperature')

    def test_1h_gap(self):
        # create samples with a gap

        start = datetime(2013, 3, 1)
        # 1 hour of data
        for minutes in range(0, 60, 2):
            dt = start + timedelta(minutes=minutes)
            sr = SensorReading(timestamp=dt, value=1.0, sensor=self.sensor, channel=self.channel)
            sr.save()

        # 1 hour gap
        restart = start + timedelta(hours=1 + 1)

        # 1 more hour of data
        for minutes in range(0, 60, 2):
            dt = restart + timedelta(minutes=minutes)
            sr = SensorReading(timestamp=dt, value=1.0, sensor=self.sensor, channel=self.channel)
            sr.save()

        readings_before = len(list(SensorReading.objects.filter(sensor=self.sensor)))
        self.assertEqual(readings_before, 60)

        # fix data gap
        fix_data_gaps(self.sensor, self.channel)

        readings_after = len(list(SensorReading.objects.filter(sensor=self.sensor)))
        self.assertEqual(readings_after, 90)

    def test_gap_values_energy(self):
        # create samples with a gap

        start = datetime(2013, 3, 1)
        # First reading
        dt = start + timedelta(minutes=0)
        sr = SensorReading(timestamp=dt, value=2.0, sensor=self.sensor, channel=self.channel)
        sr.save()

        # Second reading
        dt = start + timedelta(minutes=4)
        sr = SensorReading(timestamp=dt, value=6.0, sensor=self.sensor, channel=self.channel)
        sr.save()

        readings_before = list(SensorReading.objects.filter(sensor=self.sensor))
        self.assertEqual(len(readings_before), 2)

        # fix data gap
        fix_data_gaps(self.sensor, self.channel)

        readings_after = list(SensorReading.objects.filter(sensor=self.sensor))
        self.assertEqual(len(readings_after), 3)
        self.assertEqual(readings_after[1].value, 3)

    def test_gap_values_other(self):
        # create samples with a gap

        start = datetime(2013, 3, 1)
        # First reading
        dt = start + timedelta(minutes=0)
        sr = SensorReading(timestamp=dt, value=2.0, sensor=self.sensor, channel=self.o_channel)
        sr.save()

        # Second reading
        dt = start + timedelta(minutes=4)
        sr = SensorReading(timestamp=dt, value=6.0, sensor=self.sensor, channel=self.o_channel)
        sr.save()

        readings_before = list(SensorReading.objects.filter(sensor=self.sensor))
        self.assertEqual(len(readings_before), 2)

        # fix data gap
        fix_data_gaps(self.sensor, self.o_channel)

        readings_after = list(SensorReading.objects.filter(sensor=self.sensor))
        self.assertEqual(len(readings_after), 3)
        self.assertEqual(readings_after[1].value, 2)
